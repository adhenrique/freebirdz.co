<?php

	$query_info_contatos = "SELECT * FROM contatos";
	$result_info_contatos = mysql_query($query_info_contatos) or die (mysql_error());
	$vet_info_contatos = mysql_fetch_array($result_info_contatos);

	$query_textos_index = "SELECT * FROM textos_index";
	$result_textos_index = mysql_query($query_textos_index) or die (mysql_error());
	$vet_textos_index = mysql_fetch_array($result_textos_index);
	
	$vet_url = explode("/", $_SERVER['REQUEST_URI']);
	$url_pt = '';
	$url_en = '';

	for($i = 0; $i < count($vet_url); $i++)
	{
		if(strstr($vet_url[$i], ".php"))
		{
			$url_pt = $vet_url[$i];

			$url_en = str_replace("projeto", "project", $vet_url[$i]);
			$url_en = 'en/'.$url_en;
		}
		else
		{
			$url_pt = 'index.php';
			$url_en = 'en/index.php';
		}
	}

?>
<!doctype html>
<html>
	<head>
	    <meta charset="utf-8">
	    <link rel="shortcut icon" type="image/png" href="img/favicon.png" />
	    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=0">

	    <!-- Cor do Tema no Navegador Mobile -->
	    <meta name="theme-color" content="#fff">
		<meta name="apple-mobile-web-app-status-bar-style" content="#fff">
		<meta name="msapplication-navbutton-color" content="#fff">

		<title><? echo( $title == "" ? "Free Birdz | Conheça o nosso Portfólio" : $title );?></title>
		
		<link rev="made" href="mailto:contato@gabrielprogramador.com.br" />
		<meta name="company" content="Free Birdz" />
		<meta name="autor" content="Gabriel Programador" />
		<meta name="title" content="<? echo( $title == "" ? "Free Birdz | Conheça o nosso Portfólio" : $title );?>" />
		<meta name="description" content="<? echo( $descricao == "" ? $vet_textos_index['titulo_topo'] . " - " . limitarTexto($vet_textos_index['texto_topo'], 60) : limitarTexto($descricao,90) );?>" />
		<meta name="url" content="<? echo($url == ""? "http://freebirdz.co/" : $url);?>" />
		<meta property="og:locale" content="pt_BR">
		<meta property="og:url" content="<? echo($url == ""? "http://freebirdz.co/" : $url);?>">
		<meta property="og:title" content="<? echo( $title == "" ? "Free Birdz | Conheça o nosso Portfólio" : $title );?>">
		<meta property="og:site_name" content="Free Birdz">
		<meta property="og:description" content="<? echo( $descricao == "" ? $vet_textos_index['titulo_topo'] . " - " . limitarTexto($vet_textos_index['texto_topo'], 60) : limitarTexto($descricao,90) );?>">
		<meta property="og:image" content="http://freebirdz.co/img/img_site.jpg">
		<meta property="og:image:type" content="image/jpeg">
		<meta property="og:image:width" content="900">
		<meta property="og:image:height" content="675">
		<meta name="keywords" content="free birdz, ">

		<!-- Estilos CSS do Site -->
	    <link rel="stylesheet" type="text/css" href="css/bootstrap.css" />
	    <link rel="stylesheet" type="text/css" href="css/style.css" />
	    <link rel="stylesheet" type="text/css" href="css/animate.css" />
	    <link rel="stylesheet" type="text/css" href="css/font-awesome.css" />

	    <!-- Fontes do Site -->
	    <link rel="stylesheet" type="text/css" href="css/font-face.css" />

	    <!-- Scripts de JavaScript do Site -->
	    <script src="js/jquery-2.1.0.js"></script>
	    <script src="js/bootstrap.js"></script>
	    <script src="js/jquery.matchHeight.js"></script>
	    <script src="js/wow.min.js"></script>
	    <script src="js/TweenMax.min.js"></script>
	    <script src="js/jquery-parallax.js"></script>
	    <script src="js/jquery.stellar.js"></script>
	    <script src="js/scripts.js"></script>

	    <!-- Global site tag (gtag.js) - Google Analytics -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=UA-142786878-1"></script>
		<script>
		  window.dataLayer = window.dataLayer || [];
		  function gtag(){dataLayer.push(arguments);}
		  gtag('js', new Date());

		  gtag('config', 'UA-142786878-1');
		</script>

		<!-- Yandex.Metrika counter -->
		<script type="text/javascript" >
		   (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
		   m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
		   (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

		   ym(54208777, "init", {
		        clickmap:true,
		        trackLinks:true,
		        accurateTrackBounce:true,
		        webvisor:true
		   });
		</script>
		<noscript><div><img src="https://mc.yandex.ru/watch/54208777" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
		<!-- /Yandex.Metrika counter -->

	    
	</head>
	<body>
