
		<div class="bloco rodape" id="newsletter">
			<div class="container">
				<div class="row">
					<div class="col-sm-6 col-sm-offset-3">
						<h3><?=$vet_textos_index['titulo_newsletter'];?></h3>
						<p><?=nl2br(str_replace("  ", "&nbsp;&nbsp;", $vet_textos_index['texto_newsletter']));?></p>
						<form method="post" class="row" id="form-newsletter">
                            <input type="hidden" name="cmd" value="newsletter" />
                            <img src="img/icon-origami-form-newsletter.png" class="origami" />
                            <img src="img/icon-carta-input-newsletter.png" class="carta-input" />
                            <div class="col-sm-12 flex">
                                <input type="email" name="email" class="form-control" placeholder="contact@freebirdz.co" required />
                                <span class="submit-group">
                                    <div class="loader"></div>
                                    <button class="btn btn-default">
                                        <img src="img/icon-btn-aviao.svg" class="svg" />
                                    </button>
                                </span>
                            </div>
						</form>
					</div>
				</div>
			</div>
		</div>

		<div class="bloco rodape" id="informacoes-contato">
			<div class="container-fluid">
				<div class="row">
					<div class="col-sm-3 logo" data-mh="height-col-informacoes">
						<div id="logo-cores" class="carousel slide carousel-fade" data-interval="1500" data-ride="carousel">

							<div class="carousel-inner">

								<div class="item filme active">
									<img src="img/icon-rodape-filme.svg" class="svg" />
								</div>
								<div class="item design">
									<img src="img/icon-rodape-design.svg" class="svg" />
								</div>
								<div class="item musica">
									<img src="img/icon-rodape-musica.svg" class="svg" />
								</div>

							</div>
						</div>
					</div>
					<div class="col-sm-9 informacoes" data-mh="height-col-informacoes">
						<div class="row">
							<div class="col-sm-4 unidade" data-mh="height-col-informacoes-unidade-emails">
								<div class="icon-line"><img src="img/icon-marker-rodape.svg" class="svg" /></div>
								<h4><?=$vet_info_contatos['titulo_endereco_1'];?></h4>
								<a href="tel:<?=$vet_info_contatos['telefone_1'];?>"><?=$vet_info_contatos['telefone_1'];?></a><br>
								<p><?=$vet_info_contatos['endereco_1'];?></p>
							</div>
							<div class="col-sm-4 unidade" data-mh="height-col-informacoes-unidade-emails">
								<h4><?=$vet_info_contatos['titulo_endereco_2'];?></h4>
								<a href="tel:<?=$vet_info_contatos['telefone_2'];?>"><?=$vet_info_contatos['telefone_2'];?></a><br>
								<p><?=$vet_info_contatos['endereco_2'];?></p>
							</div>
							<div class="col-sm-4 emails" data-mh="height-col-informacoes-unidade-emails">
								<div class="icon-line"><img src="img/icon-envelope-rodape.svg" class="svg" /></div>
								<h4>CONTATO</h4>
								<?
									if($vet_info_contatos['email_1'] != ''){
								?>
									<div class="email">
										<?
											$email_1 = explode("@", $vet_info_contatos['email_1']);
										?>
										<a href="mailto:<?=$vet_info_contatos['email_1'];?>"><b><?=$email_1[0];?></b>@<?=$email_1[1];?></a>
									</div>
								<?
									}
									if($vet_info_contatos['email_2'] != ''){
								?>
									<div class="email">
										<?
											$email_2 = explode("@", $vet_info_contatos['email_2']);
										?>
										<a href="mailto:<?=$vet_info_contatos['email_2'];?>"><b><?=$email_2[0];?></b>@<?=$email_2[1];?></a>
									</div>
								<?
									}
									if($vet_info_contatos['email_3'] != ''){
								?>
									<div class="email">
										<?
											$email_3 = explode("@", $vet_info_contatos['email_3']);
										?>
										<a href="mailto:<?=$vet_info_contatos['email_3'];?>"><b><?=$email_3[0];?></b>@<?=$email_3[1];?></a>
									</div>
								<?
									}
								?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

        <?php include_once "{$configs['root']}/parts/bottom_social_icons.php";?>

		<div class="bloco rodape" id="direitos">
			<div class="container">
				<div class="row">
					<div class="col-sm-12 text-center">
						<p>©2007–2019 Free Birdz. Todos os direitos reservados.</p>
					</div>
				</div>
			</div>
		</div>

		<div id="idiomas">
			<a href="<?=$url_pt;?>">
                <img src="img/languageicon_br01.svg" />
<!--                <img src="img/languageicon_br02.svg" class="hover" />-->
            </a>
			<a href="<?=$url_en;?>">
                <img src="img/languageicon_eua02.svg" class="normal" />
                <img src="img/languageicon_eua01.svg" class="hover" />
            </a>
		</div>

		<?php include_once "{$configs['root']}/parts/modals.php" ?>
        <?php include_once "{$configs['root']}/parts/scripts.php" ?>

	</body>
</html>
