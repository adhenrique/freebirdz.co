function alturaBG(item, prop){
    tamanhoImagemBG = $(item).width();
    $(item).css("height", tamanhoImagemBG/prop);
    $( window ).resize(function() {
        tamanhoImagemBG = $(item).width();
        $(item).css("height", tamanhoImagemBG/prop);
    })
}

function scrollPage(element){
    $("html, body").animate({
        scrollTop: $(element).offset().top - 50
    },600)
}

function addCurtida(id_portfolio, idioma){

    var curtidas = parseInt($(".cont-curtidas#portfolio-" + id_portfolio).text());

    // dados a enviar ao PHP pelo AJAX
    var dadosajax = {
        'id_portfolio' : id_portfolio
    };
    var page_url = 'curtida.php';
    if(idioma == "en"){
        var page_url = '../curtida.php';
    }

    $.ajax({

        // url da página PHP
        url: page_url,
        // parametros a serem passados para a página
        data: dadosajax,
        // tipo de dados e forma de serem enviados
        type: 'POST',
        // cache
        cache: false,

        error: function(){
            alert('Error na função!');
        },
        success: function(result){
            if($.trim(result) == '0'){

                var curtidas_finais = curtidas + 1;

                $(".cont-curtidas#portfolio-" + id_portfolio + " font").text(curtidas_finais);
                $(".cont-curtidas#portfolio-" + id_portfolio + " img").attr('src', 'http://freebirdz.co/img/icon-coracao-curtido.svg');

            }
            else if($.trim(result) == '1'){

                var curtidas_finais = curtidas - 1;

                $(".cont-curtidas#portfolio-" + id_portfolio + " font").text(curtidas_finais);
                $(".cont-curtidas#portfolio-" + id_portfolio + " img").attr('src', 'http://freebirdz.co/img/icon-coracao.svg');

            }
        }

    })

}

let modalTimer;
function modalAlerta(titulo, texto, type, modalID = '#modal-alerta'){
    let modal = $(modalID);
    modal.modal('show');
    clearTimeout(modalTimer);

    modal.removeClass('success');
    modal.removeClass('error');
    modal.addClass(type);

    modal.find('h4.modal-title').html(titulo);
    modal.find('.modal-body p').html(texto);

    modalTimer = setTimeout(function(){
        modal.modal('hide');
    },15000)
}


$(function(){

    new WOW().init({
        mobile: false
    });

    $.stellar({
        horizontalScrolling: false,
        verticalOffset: 40
    });

    $('img.svg').each(function(){
        var $img = jQuery(this);
        var imgID = $img.attr('id');
        var imgClass = $img.attr('class');
        var imgURL = $img.attr('src');

        jQuery.get(imgURL, function(data) {
            // Get the SVG tag, ignore the rest
            var $svg = jQuery(data).find('svg');

            // Add replaced image's ID to the new SVG
            if(typeof imgID !== 'undefined') {
                $svg = $svg.attr('id', imgID);
            }
            // Add replaced image's classes to the new SVG
            if(typeof imgClass !== 'undefined') {
                $svg = $svg.attr('class', imgClass+' replaced-svg');
            }

            // Remove any invalid XML tags as per http://validator.w3.org
            $svg = $svg.removeAttr('xmlns:a');

            // Replace image with new SVG
            $img.replaceWith($svg);

        }, 'xml');
    });

    if($(window).width() > 768){

        $(".index#topo").css("height", $(window).height());
        $(".index#topo .nuvens").css("height", $(".index#topo .nuvens .pretas img").height());

        $(".rodape#informacoes-contato .logo .carousel .item").css("padding-left", ($(".rodape#informacoes-contato .container-fluid").width() - $(".container").width())/2);
        $(".rodape#informacoes-contato .informacoes").css("padding-right", ($(".rodape#informacoes-contato .container-fluid").width() - $(".container").width())/2.2);

    }
    else{

        $(".index#topo").css("height", $(window).height() * 0.95);
        $(".index#topo .nuvens").css("height", $(".index#topo .nuvens .pretas img").height()*1.5);

    }

    alturaBG(".index#portfolio .galeria .item a .bg", 1);
    alturaBG(".index#portfolio .galeria .item a .img", 1);
    // alturaBG(".portfolio-ver#conteudo .item iframe", 2);

    setTimeout(function(){

        if($(window).width() > 768){

            $(".index#topo").css("height", $(window).height());
            $(".index#topo .nuvens").css("height", $(".index#topo .nuvens .pretas img").height());

        }
        else{


            $(".index#topo").css("height", $(window).height() * 0.95);
            $(".index#topo .nuvens").css("height", $(".index#topo .nuvens .pretas img").height()*1.5);

        }
    },300)

})

$(window).resize(function(){

})

$(window).scroll(function(){

    if($(window).width() > 768){

        if($(document).scrollTop() >= $(".portfolio-ver#topo").outerHeight() - $(".bar-topo.portfolio-ver").height()){
            $(".portfolio-ver#info").addClass("fixed");
            $(".portfolio-ver#info").css("top", $(".bar-topo.portfolio-ver").height());
            $(".portfolio-ver#conteudo").css("padding-top", 80 + $(".portfolio-ver#info").outerHeight());
        }
        else{
            $(".portfolio-ver#info").removeClass("fixed");
            $(".portfolio-ver#info").css("top", 0);
            $(".portfolio-ver#conteudo").css("padding-top", 80);
        }

    }
    else{

        if($(document).scrollTop() >= $(".portfolio-ver#topo").outerHeight() - $(".bar-topo.portfolio-ver").height()){
            $(".portfolio-ver#info").addClass("fixed");
            $(".portfolio-ver#info").css("top", $(".bar-topo.portfolio-ver").height());
            $(".portfolio-ver#conteudo").css("padding-top", 40 + $(".portfolio-ver#info").outerHeight());
        }
        else{
            $(".portfolio-ver#info").removeClass("fixed");
            $(".portfolio-ver#info").css("top", 0);
            $(".portfolio-ver#conteudo").css("padding-top", 40);
        }

    }

    if((($(document).scrollTop() + $(document).height())/2) + 15 < $("#informacoes-contato").offset().top){

    }
    else{

    }

})

// $fn.parallax( resistance, mouse )
$(document).mousemove(function(e){
    $('.index#topo .nuvens .pretas').parallax(-15,e);
    $('.index#topo .nuvens .cinzas').parallax(10,e);
    $('.index#topo .nuvens .brancas').parallax(-50,e);
});
