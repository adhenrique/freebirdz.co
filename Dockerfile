FROM php:5.5.23-apache

# Installs curl
RUN docker-php-ext-install mysql

COPY . /var/www/html
WORKDIR /var/www/html

EXPOSE 80