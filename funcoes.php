﻿<?php

date_default_timezone_set('America/Sao_Paulo');

// Obter o ip do usuário
$ip = getenv("REMOTE_ADDR");

function get_client_ip(){


	$ipadress = "";

	if(isset($_SERVER['HTTP_CLIENT_IP'])){
		$ipadress = $_SERVER['HTTP_CLIENT_IP'];
	}
	elseif(isset($_SERVER['HTTP_X_FORWARDED_FOR'])){
		$ipadress = $_SERVER['HTTP_X_FORWARDED_FOR'];
	}
	elseif(isset($_SERVER['HTTP_X_FORWARDED'])){
		$ipadress = $_SERVER['HTTP_X_FORWARDED'];
	}
	elseif(isset($_SERVER['REMOTE_ADDR'])){
		$ipadress = $_SERVER['REMOTE_ADDR'];
	}
	else{
		$ipadress = "UNKNOWN";			
	}

	return $ipadress;

}


function thumbnailVimeo($id){
	$vimeo = unserialize(file_get_contents("http://vimeo.com/api/v2/video/$id.php"));
	echo $small = $vimeo[0]['thumbnail_small'];
	echo $medium = $vimeo[0]['thumbnail_medium'];
	echo $large = $vimeo[0]['thumbnail_large'];
}
function thumbnailYouTube($id) {
    $resolution = array (
        'maxresdefault',
        'sddefault',
        'mqdefault',
        'hqdefault',
        'default'
    );

    for ($x = 0; $x < sizeof($resolution); $x++) {
        $url = 'http://img.youtube.com/vi/' . $id . '/' . $resolution[$x] . '.jpg';

    }
    return $url;
}

// Data e Hora
$data_hj 	= date("d-m-Y");
$data_hora 	= date("d-m-Y H:i:s");
$data_hora_eua 	= date("Y-m-d H:i:s");

function data_hora_intervalo($intervalo){
	$hora = date("H");
	$minuto = date("i");
	if(date("i") < $intervalo){
		if(date("H") == 01){
			$minuto = 00;
		}
		else{
			$hora--;
			$intervalo = $intervalo - $minuto;
			$minuto = 59;
			$minuto = $minuto - $intervalo;
		}
	}
	else{
		$minuto = date("i") -$intervalo;
	}
	$data_hora_intervalo = date("Y-m-d "). $hora . ":" . $minuto . date(":s");
	return $data_hora_intervalo;
}

$dia_hj = date("d");
$mes_hj = date("m");
$ano_hj = date("Y");

function modalAlerta($titulo, $texto)
{
	print("<script>$(function(){modalAlerta(\"$titulo\",\"$texto\");});</script>");
}

function eventAnalytics($categoria, $acao, $etiqueta)
{
	print("<script>$(function(){ gtag('event', '$acao', { 'event_category': '$categoria', 'event_label': '$etiqueta'}); });</script>");
}

// Função de Eliminação de qalquer comando que possa invadir ou alterar o sistema de forma indevida
function anti_injection($sql){
	$sql = preg_replace(sql_regcase("/(from|select|insert|delete|where|drop table|show tables|#|\*|--|\\\\)/"), "" ,$sql); // remove palavras que contenham sintaxe sql
	$sql = trim($sql); // limpa espaços vazios
	$sql = strip_tags($sql); // tira tags html e php
	$sql = addslashes($sql); //  adiciona barras invertidas a um string
	return $sql;
}

// Função de redirecionamento de página
function redireciona($string){
	print ("<script language='JavaScript'>self.location.href=\"$string\";</script>");
	die;
}

function limitarTexto($texto, $limite){
	$contador = strlen($texto);
	if ( $contador >= $limite ) {      
		$texto = substr($texto, 0, strrpos(substr($texto, 0, $limite), ' ')) . '...';
		return $texto;
	}
	else{
		return $texto;
	}
} 

function converteAbrevMes($mes){
	if($mes == "Jan"){
		return '01';
	}
	else if($mes == "Feb"){
		return '02';
	}
	else if($mes == "Mar"){
		return '03';
	}
	else if($mes == "Apr"){
		return '04';
	}
	else if($mes == "May"){
		return '05';
	}
	else if($mes == "June"){
		return '06';
	}
	else if($mes == "July"){
		return '07';
	}
	else if($mes == "Aug"){
		return '08';
	}
	else if($mes == "Sept"){
		return '09';
	}
	else if($mes == "Oct"){
		return '10';
	}
	else if($mes == "Nov"){
		return '11';
	}
	else if($mes == "Dec"){
		echo '12';
	}
}

function preco($preco){
	if($preco > 0){
		echo "R$" . number_format($preco, 2, ',', '');
	}
	else{
		echo "R$0,00";
	}
}

?>