<?
	include("db_acesso.php");
	include("funcoes.php");

    $page = "projeto";

	if($_GET['projeto']){

		$codigo_portfolio = $_GET['projeto'];
		$codigo_categoria = $_GET['categoria'];

		if($codigo_categoria == ""){
			$query_categoria_inicial = "SELECT * FROM portfolio_categorias WHERE codigo_portfolio = '$codigo_portfolio' ORDER BY codigo ASC LIMIT 1";
			$result_categoria_inicial = mysql_query($query_categoria_inicial) or die(mysql_error());
			$vet_categoria_inicial = mysql_fetch_array($result_categoria_inicial);
			$codigo_categoria = $vet_categoria_inicial['codigo'];
		}

		$query_portfolio = "SELECT * FROM portfolio WHERE codigo = '$codigo_portfolio' LIMIT 1";
		$result_portfolio = mysql_query($query_portfolio) or die(mysql_error());
		$row_portfolio = mysql_num_rows($result_portfolio);

		if($row_portfolio > 0){
			$vet_portfolio = mysql_fetch_array($result_portfolio);
		}
		else{
			redireciona("index.php");
		}

		$query_categoria = "SELECT * FROM portfolio_categorias WHERE codigo_portfolio = '$codigo_portfolio' AND codigo = '$codigo_categoria'";
		$result_categoria = mysql_query($query_categoria) or die(mysql_error());
		$vet_categoria = mysql_fetch_array($result_categoria);


		$proximoPortfolio = mysql_query("SELECT * FROM portfolio WHERE codigo > '$codigo_portfolio' ORDER BY codigo ASC LIMIT 1") or die(mysql_error());
		if(mysql_num_rows($proximoPortfolio) == 0){
			$proximoPortfolio = mysql_query("SELECT * FROM portfolio ORDER BY codigo ASC LIMIT 1") or die(mysql_error());
		}
		$vet_proximoPortfolio = mysql_fetch_array($proximoPortfolio);
		$urlProximoPortfolio = "http://freebirdz.co/projeto-ver.php?projeto=" . $vet_proximoPortfolio['codigo'];

		$anteriorPortfolio = mysql_query("SELECT * FROM portfolio WHERE codigo < '$codigo_portfolio' ORDER BY codigo DESC LIMIT 1") or die(mysql_error());
		if(mysql_num_rows($anteriorPortfolio) == 0){
			$anteriorPortfolio = mysql_query("SELECT * FROM portfolio ORDER BY codigo DESC LIMIT 1") or die(mysql_error());
		}
		$vet_anteriorPortfolio = mysql_fetch_array($anteriorPortfolio);
		$urlAnteriorPortfolio = "http://freebirdz.co/projeto-ver.php?projeto=" . $vet_anteriorPortfolio['codigo'];

	}
	else{
		redireciona("index.php");
	}


	// Definições do Cookie de Visita
	$horas = 6;
	$name_cookie = "contador_visita" . $codigo_portfolio;

	// Comando para setar o cookie
	setcookie($name_cookie, true, time() + (3600 * $horas));

	// Verificação do cookie
	if(isset($_COOKIE[$name_cookie]) && $_COOKIE[$name_cookie] == true){
	    // Faz nada...
	}
	else {
		$visualizacoes = $vet_portfolio['visualizacoes'] + 1;
		mysql_query("UPDATE portfolio SET visualizacoes = '$visualizacoes' WHERE codigo = '$codigo_portfolio' LIMIT 1");
	}


	$page = "portfolio-ver";
	$title = "Projeto | " . $vet_portfolio['titulo'] . " - " . $vet_categoria['titulo_categoria'];
	$descricao = $vet_portfolio['descricao'];
	$url = "http://freebirdz.co/projeto-ver.php?projeto=" . $codigo_portfolio . "&categoria=" . $vet_categoria['codigo'];
	include("includes/header.php");

	$query_curtidas = mysql_query("SELECT * FROM portfolio_curtidas WHERE codigo_portfolio = '$codigo_portfolio'");
	$qntd_curtidas = mysql_num_rows($query_curtidas);

	$ip = get_client_ip();
	$query_curtida_usuario = mysql_query("SELECT * FROM portfolio_curtidas WHERE codigo_portfolio = '$codigo_portfolio' AND ip = '$ip'");
	$curtida_usuario = mysql_num_rows($query_curtida_usuario);

?>

	<script>

		$(function(){
			$.stellar({
				horizontalScrolling: false,
				verticalOffset: 40
			});
		})

	</script>

	<div class="bar-topo portfolio-ver">
		<div class="container-fluid">
			<div class="row barras">
				<?
					$query_tipos = "SELECT * FROM portfolio_tipos WHERE codigo_portfolio = '$codigo_portfolio'";
					$result_tipos = mysql_query($query_tipos) or die(mysql_error());

					if(mysql_num_rows($result_tipos) == 1){
						$col_bar_categorias = 12;
					}
					elseif(mysql_num_rows($result_tipos) == 2){
						$col_bar_categorias = 6;
					}
					elseif(mysql_num_rows($result_tipos) == 3){
						$col_bar_categorias = 4;
					}
					else{
						$col_bar_categorias = "12 hidden";
					}
					while($vet_tipo = mysql_fetch_array($result_tipos)){
						if($vet_tipo['tipo'] == 1){ // Filme (Amarelo)
							echo "<div class=\"col-xs-" . $col_bar_categorias . " item amarelo\"></div>";
						}
						if($vet_tipo['tipo'] == 2){ // Design (Vermelho)
							echo "<div class=\"col-xs-" . $col_bar_categorias . " item vermelho\"></div>";
						}
						if($vet_tipo['tipo'] == 3){ // Música (Azul)
							echo "<div class=\"col-xs-" . $col_bar_categorias . " item azul\"></div>";
						}
					}
				?>
			</div>
		</div>
	</div>

	<div class="bloco portfolio-ver" data-stellar-background-ratio="0.6" id="topo" style="background-image: url('img/portfolio/<?=$vet_portfolio['bg_topo'];?>');">
		<div class="ruido"></div>
		<div class="container-fluid topo">
			<div class="row">
				<div class="col-sm-6 logo">
					<a href="index.php"><img src="img/logo-mini-portfolio.svg" class="img-logo svg" /></a>
					<h4>|&nbsp;&nbsp;&nbsp;&nbsp;PROJETO CONCEITUAL</h4>
				</div>
			</div>
		</div>
		<div class="container depoimento">
			<div class="row">
				<div class="col-sm-4 col-sm-offset-4">
					<p class="text-center">
						“<br>
						<?=$vet_portfolio['frase_cliente'];?>
						<br>”
					</p>
                    <h4 class="text-right">- <?=$vet_portfolio['nome_cliente'];?></h4>
				</div>
			</div>
		</div>
	</div>

	<div class="bloco portfolio-ver" id="info">
		<div class="container">
			<div class="row">
				<div class="col-sm-2 col-xs-2 col-sm-offset-1 logo">
					<img src="img/portfolio/<?=$vet_portfolio['logo'];?>" class="img-responsive" />
				</div>
				<div class="col-sm-7 col-xs-8 texto">
					<h2><?=$vet_portfolio['titulo'];?><small><img src="img/icon-marker-rodape.svg" class="svg" /> <?=$vet_portfolio['localidade'];?>&nbsp;&nbsp;&nbsp;-&nbsp;&nbsp;&nbsp;<?=$vet_portfolio['ano'];?></small></h2>
					<h3>
						<?
							$query_categorias = "SELECT * FROM portfolio_categorias WHERE codigo_portfolio = '$codigo_portfolio' ORDER BY codigo DESC";
							$result_categorias = mysql_query($query_categorias) or die(mysql_error());

							$cont_categorias = 0;

							while($vet_categorias = mysql_fetch_array($result_categorias)){

								if($cont_categorias != 0){
									echo "&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;";
								}
								if($vet_categorias['codigo'] == $codigo_categoria){
						?>
								<a href="javascript:;" class="active"><?=$vet_categorias['titulo_categoria'];?></a>
						<?
								}
								else{
						?>
								<a href="projeto-ver.php?projeto=<?=$codigo_portfolio;?>&categoria=<?=$vet_categorias['codigo'];?>"><?=$vet_categorias['titulo_categoria'];?></a>
						<?
								}
								$cont_categorias++;
							}
						?>
					</h3>
				</div>
				<div class="col-sm-1 col-xs-1 like">
					<a href="javascript:;" onclick="addCurtida(<?=$codigo_portfolio;?>, 'pt');">
						<h4 class="cont-curtidas" id="portfolio-<?=$codigo_portfolio;?>">
							<img src="img/icon-coracao<? echo($curtida_usuario > 0 ? "-curtido" : "") ;?>.svg" /> <font><?=$qntd_curtidas;?></font>
						</h4>
					</a>
				</div>
			</div>
		</div>
	</div>

	<div class="bloco portfolio-ver" id="conteudo">
		<div class="container">
			<div class="row">
				<div class="col-sm-10 col-sm-offset-1 text-center">
					<?
						$query_conteudo = "SELECT * FROM portfolio_conteudo WHERE codigo_categoria = '$codigo_categoria' AND codigo_portfolio = '$codigo_portfolio' ORDER BY codigo ASC";
						$result_conteudo = mysql_query($query_conteudo) or die(mysql_error());
						$row_conteudo = mysql_num_rows($result_conteudo);

						if($row_conteudo > 0){
							while($vet_conteudo = mysql_fetch_array($result_conteudo)){
					?>
						<div class="item">
                            <h2><?=$vet_conteudo['titulo'];?></h2>
							<?
								if($vet_conteudo['imagem']){
							?>
								<img src="img/portfolio/<?=$vet_conteudo['imagem'];?>" class="img-responsive center-block" />
							<?
								}else if($vet_conteudo['gif']) {
                            ?>
                                <img src="<?= $vet_conteudo['gif']; ?>" class="img-responsive center-block"/>
                            <?
                                } else {
									if($vet_conteudo['video']){
										if(strpos($vet_conteudo['video'], "youtube")){
											$url = $vet_conteudo['video'];
										    preg_match('/[\\?\\&]v=([^\\?\\&]+)/', $url, $matches);
										    $codigo_youtube = $matches[1];
								    ?>
								    		<div class="embed-container youtube">
                                                <iframe src="<?=$codigo_youtube;?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                            </div>
								    <?
										}
										if(strpos($vet_conteudo['video'], "vimeo")){
											$url = $vet_conteudo['video'];
											preg_match('%^https?:\/\/(?:www\.|player\.)?vimeo.com\/(?:channels\/(?:\w+\/)?|groups\/([^\/]*)\/videos\/|album\/(\d+)\/video\/|video\/|)(\d+)(?:$|\/|\?)(?:[?]?.*)$%im', $url ,$matches);
											$codigo_vimeo = $matches[3];
									?>
											<div class="embed-container vimeo">
                                                <iframe src="https://player.vimeo.com/video/<?=$codigo_vimeo;?>" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
                                            </div>
									<?
										}
									}
								}
							?>
							<p>
								<?=$vet_conteudo['texto'];?>
							</p>
						</div>
					<?
							}
						}
					?>
				</div>
			</div>
		</div>
	</div>

	<a href="<?=$urlProximoPortfolio;?>" class="portfolio-ver seta-alterar direita">
		<img src="img/seta-direita-portfolio_new.svg" class="svg" />
	</a>

	<a href="<?=$urlAnteriorPortfolio;?>" class="portfolio-ver seta-alterar esquerda">
		<img src="img/seta-esquerda-portfolio_new.svg" class="svg" />
	</a>

<?
	include("includes/footer.php");
?>
