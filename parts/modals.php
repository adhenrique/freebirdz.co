<!-- Modal -->
<div class="modal fade" id="modal-alerta" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <div class="modal-header-img"></div>
            </div>
            <div class="modal-body">
                <h4 class="modal-title" id="myModalLabel"></h4>
                <p></p>
                <button type="button" class="btn modal-button-close" data-dismiss="modal" aria-label="Close">
                    Continue
                </button>
            </div>
        </div>
    </div>
</div>

<!-- Modal Construção -->
<div class="modal fade" id="modal-alerta-construcao" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <div class="modal-header-img"></div>
            </div>
            <div class="modal-body">
                <p></p>
                <img src="../img/color_dot.png" alt="">
                <button type="button" class="btn modal-button-close" data-dismiss="modal" aria-label="Close">
                    Continue
                </button>
            </div>
        </div>
    </div>
</div>
