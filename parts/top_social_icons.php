<div class="row">
    <div class="col-sm-4 col-sm-offset-4 col-xs-12 item redes-sociais text-center">
        <?
        if($vet_info_contatos['link_facebook']){
            echo "<a href=\"" . $vet_info_contatos['link_facebook'] . "\" target=\"_blank\" class=\"wow fadeInDown\" data-wow-delay=\".2s\" data-wow-duration=\"1s\"><img src=\"/img/icon-facebook.svg\" class=\"svg\" /></a>";
        }
        if($vet_info_contatos['link_instagram']){
            echo "<a href=\"" . $vet_info_contatos['link_instagram'] . "\" target=\"_blank\" class=\"wow fadeInDown\" data-wow-delay=\".2s\" data-wow-duration=\"1s\"><img src=\"/img/icon-instagram.svg\" class=\"svg\" /></a>";
        }
        if($vet_info_contatos['link_twitter']){
            echo "<a href=\"" . $vet_info_contatos['link_twitter'] . "\" target=\"_blank\" class=\"wow fadeInDown\" data-wow-delay=\".2s\" data-wow-duration=\"1s\"><img src=\"/img/icon-twitter.svg\" class=\"svg\" /></a>";
        }
        if($vet_info_contatos['link_vimeo']){
            echo "<a href=\"" . $vet_info_contatos['link_vimeo'] . "\" target=\"_blank\" class=\"wow fadeInDown\" data-wow-delay=\".2s\" data-wow-duration=\"1s\"><img src=\"/img/icon-vimeo.svg\" class=\"svg\" /></a>";
        }
        if($vet_info_contatos['link_behance']){
            echo "<a href=\"" . $vet_info_contatos['link_behance'] . "\" target=\"_blank\" class=\"wow fadeInDown\" data-wow-delay=\".2s\" data-wow-duration=\"1s\"><img src=\"/img/icon-behance.svg\" class=\"svg\" /></a>";
        }
        if($vet_info_contatos['link_pinterest']){
            echo "<a href=\"" . $vet_info_contatos['link_pinterest'] . "\" target=\"_blank\" class=\"wow fadeInDown\" data-wow-delay=\".2s\" data-wow-duration=\"1s\"><img src=\"/img/icon-pinterest.svg\" class=\"svg\" /></a>";
        }
        if($vet_info_contatos['link_apple']){
            echo "<a href=\"" . $vet_info_contatos['link_apple'] . "\" target=\"_blank\" class=\"wow fadeInDown\" data-wow-delay=\".2s\" data-wow-duration=\"1s\"><img src=\"/img/icon-apple.svg\" class=\"svg\" /></a>";
        }
        if($vet_info_contatos['link_dribbble']){
            echo "<a href=\"" . $vet_info_contatos['link_dribbble'] . "\" target=\"_blank\" class=\"wow fadeInDown\" data-wow-delay=\".2s\" data-wow-duration=\"1s\"><img src=\"/img/icon-dribbble.svg\" class=\"svg\" /></a>";
        }
        ?>
    </div>
</div>
