<script>
    $(document).ready(function () {

        let request;
        let submitGroup = $('.submit-group');

        $('#form-newsletter').submit(function(event) {
            event.preventDefault();
            let form = $(this);

            if (request) {
                request.abort();
            }

            let serializedData = form.serialize();

            request = $.ajax({
                method: 'POST',
                url: '/newsletter.php',
                beforeSend: function() {
                    submitGroup.addClass('loading');
                    submitGroup.find('button').attr('disabled', true);
                },
                data: serializedData,
            });

            request.done(function (response, textStatus, jqXHR){
                submitGroup.removeClass('loading');
                submitGroup.find('button').attr('disabled', false);
                if (response === 'ok') {
                    $(function(){ modalAlerta('OBRIGADO!', 'Seu e-mail foi cadastrado com sucesso', 'success'); });
                } else if (response === 'exists') {
                    $(function(){ modalAlerta('JÁ TEMOS SEU E-MAIL', 'Seu e-mail já está cadastrado em nossa newsletter', 'error'); });
                }
            });

            request.fail(function (jqXHR, textStatus, errorThrown){
                submitGroup.removeClass('loading');
                submitGroup.find('button').attr('disabled', false);
                $(function(){ modalAlerta('ERRO', 'Ocorreu um erro ao cadastrar seu e-mail. Tente novamente', 'error'); });
            });
        });
    })
</script>
