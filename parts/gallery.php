<div class="row galeria">
    <?

    $query_portfolio = "SELECT * FROM portfolio ORDER BY codigo DESC";
    $result_portfolio = mysql_query($query_portfolio) or die(mysql_error());
    $row_portfolio = mysql_num_rows($result_portfolio);

    if($row_portfolio > 0){

        while($vet_portfolio = mysql_fetch_array($result_portfolio)){

            $codigo_portfolio = $vet_portfolio['codigo'];

            $query_categoria = "SELECT * FROM portfolio_categorias WHERE codigo_portfolio = '$codigo_portfolio' ORDER BY codigo DESC LIMIT 1";
            $result_categoria = mysql_query($query_categoria) or die(mysql_error());
            $vet_categoria = mysql_fetch_array($result_categoria);
            ?>
            <div class="col-lg-4 col-xs-6 item wow fadeIn" data-wow-delay=".4s" data-wow-duration="1.5s">
                <?php if($language === "br"): ?>
                    <a href="projeto-ver.php?projeto=<?=$codigo_portfolio;?>&categoria=<?=$vet_categoria['codigo'];?>">
                <?php else: ?>
                    <a href="project-ver.php?project=<?=$codigo_portfolio;?>&categoria=<?=$vet_categoria['codigo'];?>">
                <?php endif; ?>
                    <div class="bar-categorias">
                        <div class="row">
                            <?
                            $query_tipos = "SELECT * FROM portfolio_tipos WHERE codigo_portfolio = '$codigo_portfolio'";
                            $result_tipos = mysql_query($query_tipos) or die(mysql_error());

                            $col_bar_categorias = "12 hidden";
                            if(mysql_num_rows($result_tipos) == 1){
                                $col_bar_categorias = 12;
                            }
                            elseif(mysql_num_rows($result_tipos) == 2){
                                $col_bar_categorias = 6;
                            }
                            elseif(mysql_num_rows($result_tipos) == 3){
                                $col_bar_categorias = 4;
                            }

                            $count = 1;
                            while($vet_tipo = mysql_fetch_array($result_tipos)){
                                if($vet_tipo['tipo'] == 1){ // Filme (Amarelo)
                                    echo "<div class=\"col-xs-" . $col_bar_categorias . " bar time-{$count} amarelo\"></div>";
                                }
                                if($vet_tipo['tipo'] == 2){ // Design (Vermelho)
                                    echo "<div class=\"col-xs-" . $col_bar_categorias . " bar time-{$count} vermelho\"></div>";
                                }
                                if($vet_tipo['tipo'] == 3){ // Música (Azul)
                                    echo "<div class=\"col-xs-" . $col_bar_categorias . " bar time-{$count} azul\"></div>";
                                }
                                $count += 1;
                            }
                            ?>
                        </div>
                    </div>
                    <?
                    if($vet_portfolio['tipo_capa'] == 1){
                        ?>
                        <div class="bg" style="background-image: url('/img/portfolio/<?=$vet_portfolio['img_capa'];?>');"></div>
                        <?
                    }
                    elseif($vet_portfolio['tipo_capa'] == 2){
                        ?>
                        <div class="img">
                            <img src="/img/portfolio/<?=$vet_portfolio['img_capa'];?>" />
                        </div>
                        <?
                    }
                    ?>
                </a>
            </div>
            <?
        }
    }
    ?>

</div>
