<!-- Main navbar -->
<div class="navbar navbar-inverse navbar-fixed-top">
	<div class="navbar-header">
		<a class="navbar-brand" href="index.php"><img src="assets/images/logo2.png" alt=""></a>

		<ul class="nav navbar-nav visible-xs-block">
			<li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-user"></i></a></li>
			<li><a class="sidebar-mobile-main-toggle"><i class="icon-paragraph-justify3"></i></a></li>
		</ul>
	</div>

	<div class="navbar-collapse collapse" id="navbar-mobile">
		<ul class="nav navbar-nav">
			
		</ul>

		<a href="../index.php" class="navbar-text" target="_blank"><span class="label bg-info-700">IR PARA O SITE</span></a>

		<ul class="nav navbar-nav navbar-right">
			
			<li class="dropdown dropdown-user">
				<a class="dropdown-toggle" data-toggle="dropdown">
					<img src="<?php echo $_SESSION['adm_perfil']; ?>" class="img-circle img-sm" alt="">
					<span><?php echo $_SESSION['adm_nome']; ?></span>
					<i class="caret"></i>
				</a>

				<ul class="dropdown-menu dropdown-menu-right">
					<li><a href="usuarios_edit.php?edit=<?php echo $_SESSION['adm_codigo']; ?>"><i class="icon-user"></i> Meu Perfil</a></li>
					<li><a href="logout.php"><i class="icon-switch2"></i> Desconectar</a></li>
				</ul>
			</li>
		</ul>
	</div>
</div>
<!-- /main navbar -->