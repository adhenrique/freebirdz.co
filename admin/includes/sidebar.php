<!-- Main sidebar -->
<div class="sidebar sidebar-main sidebar-fixed">
	<div class="sidebar-content">

		<!-- User menu -->
		<div class="sidebar-user">
			<div class="category-content">
				<div class="media">
					<a href="#" class="media-left"><img src="<?php echo $_SESSION['adm_perfil']; ?>" class="img-circle img-sm" alt=""></a>
					<div class="media-body">
						<span class="media-heading text-semibold"><?php 
							$sobrenome = explode(" ", $_SESSION['adm_nomecompleto']);
							echo $_SESSION['adm_nome'] . ' ' . $sobrenome[1];
						?></span>
						<div class="text-size-mini text-muted">
							<i class="icon-user text-size-small"></i> &nbsp;<?php echo $_SESSION['adm_login']; ?>
						</div>
					</div>

					<div class="media-right media-middle">
						<ul class="icons-list">
							<li>
								<a href="usuarios_edit.php?edit=<?php echo $_SESSION['adm_codigo']; ?>"><i class="icon-cog3"></i></a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<!-- /user menu -->


		<!-- Main navigation -->
		<div class="sidebar-category sidebar-category-visible">
			<div class="category-content no-padding">
				<ul class="navigation navigation-main navigation-accordion">

					<!-- Menu -->
					<li class="navigation-header"><span>Menu</span> <i class="icon-menu" title="Main pages"></i></li>
					<li id="link_sidebar_dashboard"><a href="dashboard.php"><i class="icon-home4"></i> <span>Início</span></a></li>
					<li id="link_sidebar_textos_home"><a href="textos-home.php"><i class="icon-list"></i> <span>Textos da Home</span></a></li>
					<li id="link_sidebar_info_contatos"><a href="info-contatos.php"><i class="icon-phone2"></i> <span>Informações de Contato</span></a></li>
					<li id="link_sidebar_portfolio"><a href="portfolio.php"><i class="icon-grid"></i> <span>Portfólio</span></a></li>
					<li id="link_sidebar_newsletter"><a href="newsletter.php"><i class="icon-newspaper"></i> <span>Newsletter</span></a></li>
					<?
						if($_SESSION['admin'] == 1){
					?>
						<li id="link_sidebar_usuarios"><a href="usuarios.php"><i class="icon-users4"></i> <span>Usuários</span></a></li>
					<?
						}
					?>
				
				</ul>
			</div>
		</div>
		<!-- /main navigation -->

	</div>
</div>
<!-- /main sidebar -->