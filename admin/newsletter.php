<?php
	session_start();

	if($_GET['exportar'] == "csv"){
		
		include("../db_acesso.php");

		header('Content-Type: text/csv; charset=utf-8');  
		header('Content-Disposition: attachment; filename=leads_free_birdz.csv');  
		$output = fopen("php://output", "w");  
		fputcsv($output, array('ID', 'E-mail', 'Idioma', 'Data'));  
		$query = "SELECT codigo, email, idioma, data FROM newsletter ORDER BY codigo ASC";  
		$result = mysql_query($query);  
		while($row = mysql_fetch_assoc($result))  {  
		   fputcsv($output, $row);  
		}  
		fclose($output);
		die;

	}

	$include_js = '
	<script type="text/javascript" src="assets/js/plugins/media/fancybox.min.js"></script>

	<script type="text/javascript" src="assets/js/pages/user_pages_team.js"></script>

	<script type="text/javascript">
		$(function(){
			$("#link_sidebar_newsletter").addClass("active");
		});
	</script>';
	$include_ccs = "";
	include("includes/header.php");
	include("includes/verifica.php");

?>

<body class="navbar-top">
	<?php
		// Inseri a barra fixa do topo
		include("includes/navbar-top.php");
	?>
	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">
			<?php
				// Inseri a barra fixa do topo
				include("includes/sidebar.php");
			?>	
			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Page header -->
				<div class="page-header">
					<div class="page-header-content">
						<div class="page-title">
							<h4><span class="text-semibold">Leads de Newsletter</span></h4>
						</div>

						<div class="heading-elements">
							<div class="heading-btn-group">
								<a href="newsletter.php?exportar=csv"><button type="button" class="btn btn-labeled bg-teal-300"><b><i class="icon-file-download2"></i></b> Exportar CSV</button></a>
							</div>
						</div>
					</div>
				</div>
				<!-- /page header -->


				<!-- Content area -->
				<div class="content">
					<div class="row">
						<div class="col-sm-12">
							<div class="panel">
								<div class="panel-body">
									<h4 class="text-light">Leads Captados no Site</h4>
								</div>
								<?
									$query_leads = "SELECT * FROM newsletter ORDER BY codigo DESC";
									$result_leads = mysql_query($query_leads) or die(mysql_error());
									$row_leads = mysql_num_rows($result_leads);

									if($row_leads > 0){
								?>
								<table class="table datatable-show-all">
									<thead>
										<tr>
											<th class="text-center">ID</th>
											<th>E-mail</th>
											<th class="text-center">Idioma</th>
											<th>Data</th>
										</tr>
									</thead>
									<tbody>
										<?php
											while($vet_leads = mysql_fetch_array($result_leads)){

												$data_hora = explode(" ", $vet_leads['data']);

												$data = explode("-", $data_hora[0]);
												$data = $data[2] . "/" . $data[1] . "/" . $data[0];

												$hora = $data_hora[1];

												$data_hora = $data . " " . $hora;
										?>
										<tr>
											<td class="text-center">
												<?php echo $vet_leads['codigo']; ?>
											</td>
											<td>
												<?php echo $vet_leads['email']; ?>
											</td>
											<td class="text-center">
												<?
													if($vet_leads['idioma'] == "pt"){
														echo "<img src=\"assets/images/baneira-pt.png\" width=\"25px;\" />";
													}
													elseif($vet_leads['idioma'] == "en"){
														echo "<img src=\"assets/images/baneira-en.png\" width=\"25px;\" />";
													}
												?>
											</td>
											<td>
												<?php echo $data_hora; ?>
											</td>
										</tr>
										<?php
												}
										?>
									</tbody>
								</table>
								<?
									}
									else{
								?> 
									<div class="row" style="margin-left:10px;">
										<div class="col-lg-4 col-md-6">
											<div class="alert alert-bordered alert-danger">
												Nenhum lead capturado até o momento.
									  		</div>
									  	</div>
									</div>
							  	<?
								  }
							  	?>
							</div>
						</div>
					</div>	

				</div>

<?php
	include("includes/footer.php");
?>