<?php
	session_start();

	$include_js = '
	<script type="text/javascript" src="assets/js/plugins/media/fancybox.min.js"></script>
	<script type="text/javascript" src="assets/js/plugins/notifications/bootbox.min.js"></script>

	<script type="text/javascript" src="assets/js/pages/user_pages_team.js"></script>

	<script type="text/javascript">
		$(function(){
			$("#link_sidebar_portfolio").addClass("active");
		});
	</script>';
	$include_css = "
		<style>

			.bg-capa, .img-capa{
				width: 100%;
				background-position: center;
				background-size: cover;
				position: relative;
				text-align: center;
			}
			.img-capa img{
				display: inline-block;
				position: relative;
				max-height: 80%;
				max-width: 80%;
				top: 50%;
				transform: translateY(-50%);
			}
			.circle_tipo{
				height: 10px;
				width: 10px;
				border-radius: 20px;
				margin: 0 3px;
				display: inline-block;
			}

		</style>
	";
	include("includes/header.php");
	include("includes/verifica.php");

	if($_POST['cmd'] == "nova_categoria"){

		$codigo_portfolio = $_POST['codigo_portfolio'];
		$titulo_categoria = $_POST['titulo_categoria'];
		$titulo_categoria_en = $_POST['titulo_categoria_en'];

		mysql_query("INSERT INTO portfolio_categorias (codigo, codigo_portfolio, titulo_categoria, titulo_categoria_en) VALUES (0, '$codigo_portfolio', '$titulo_categoria', '$titulo_categoria_en')");

	}


	if($_POST['cmd'] == "edit_categoria"){

		$codigo_portfolio = $_POST['codigo_portfolio'];
		$codigo_categoria = $_POST['codigo_categoria'];
		$titulo_categoria = $_POST['titulo_categoria'];
		$titulo_categoria_en = $_POST['titulo_categoria_en'];

		mysql_query("UPDATE portfolio_categorias SET titulo_categoria = '$titulo_categoria', titulo_categoria_en = '$titulo_categoria_en' WHERE codigo = '$codigo_categoria' AND codigo_portfolio = '$codigo_portfolio' LIMIT 1");

	}


	if(isset($_GET['delet'])){
		$id_delet = $_GET['delet'];
		
		$query_portfolio = "SELECT * FROM portfolio WHERE codigo = '$id_delet'";
		$result_portfolio = mysql_query($query_portfolio) or die (mysql_error());
		$vet_portfolio = mysql_fetch_array($result_portfolio);

		if($vet_portfolio['logo']){
			unlink("../img/portfolio/" . $vet_portfolio['logo']); 		// Deletar Logo
		}
		if($vet_portfolio['img_capa']){
			unlink("../img/portfolio/" . $vet_portfolio['img_capa']); 	// Deletar Imagem de Capa
		}
		if($vet_portfolio['bg_topo']){
			unlink("../img/portfolio/" . $vet_portfolio['bg_topo']); 	// Deletar Background do Topo
		}

		$query_delet = "DELETE FROM portfolio WHERE codigo = '$id_delet'";
		$result_delet = mysql_query($query_delet) or die(mysql_error());

		mysql_query("DELETE FROM portfolio_categorias WHERE codigo_portfolio = '$id_delet'"); // Deletar Categorias
		mysql_query("DELETE FROM portfolio_tipos WHERE codigo_portfolio = '$id_delet'"); // Deletar Tipos
		mysql_query("DELETE FROM portfolio_curtidas WHERE codigo_portfolio = '$id_delet'"); // Deletar Curtidas

		$query_portfolio_conteudo = "SELECT * FROM portfolio_conteudo WHERE codigo_portfolio = '$id_delet'";
		$result_portfolio_conteudo = mysql_query($query_portfolio_conteudo) or die (mysql_error());
		while($vet_portfolio_conteudo = mysql_fetch_array($result_portfolio_conteudo)){
			if($vet_portfolio_conteudo['imagem']){
				unlink("../img/portfolio/" . $vet_portfolio_conteudo['imagem']); // Deletar Imagem do Conteúdo
			}
		}

		mysql_query("DELETE FROM portfolio_conteudo WHERE codigo_portfolio = '$id_delet'"); // Deletar Conteúdo
		
		redireciona('portfolio.php?msg_success=deleta&titulo='. $vet_portfolio['titulo']);
	}


	if(isset($_GET['delet_categoria'])){

		$id_delet_categoria = $_GET['delet_categoria'];
		$id_delet_portfolio = $_GET['portfolio'];

		$query_categoria = "SELECT * FROM portfolio_categorias WHERE codigo = '$id_delet_categoria'";
		$result_categoria = mysql_query($query_categoria) or die (mysql_error());
		$vet_categoria = mysql_fetch_array($result_categoria);

		mysql_query("DELETE FROM portfolio_categorias WHERE codigo = '$id_delet_categoria' LIMIT 1"); // Deletar Categorias

		$query_portfolio_conteudo = "SELECT * FROM portfolio_conteudo WHERE codigo_portfolio = '$id_delet'";
		$result_portfolio_conteudo = mysql_query($query_portfolio_conteudo) or die (mysql_error());
		while($vet_portfolio_conteudo = mysql_fetch_array($result_portfolio_conteudo)){
			if($vet_portfolio_conteudo['imagem']){
				unlink("../img/portfolio/" . $vet_portfolio_conteudo['imagem']); // Deletar Imagem do Conteúdo
			}
		}
		mysql_query("DELETE FROM portfolio_conteudo WHERE codigo_categoria = '$id_delet_categoria'"); // Deletar Conteúdo

		redireciona('portfolio.php?msg_success=deleta_categoria&titulo=' . $vet_categoria['titulo_categoria']);

	}


	if(isset($_GET['msg_success'])){
		$codigo_portfolio = anti_injection($_GET['codigo']);
		$query = "SELECT * FROM portfolio WHERE codigo = '$codigo_portfolio'";
		$result = mysql_query($query) or die(mysql_error());
		$vet = mysql_fetch_array($result);
		$titulo = $vet['titulo'];

		echo "<script>$(function(){ $(\".alert_success\").removeClass(\"hide\"); });</script>";
		
		if($_GET['msg_success'] == 'deleta'){
			$titulo_get = anti_injection($_GET['titulo']);
			echo "<script>$(function(){ $(\".alert_success font\").html(\"Portfólio <span class='text-semibold'>" . $titulo_get . "</span> deletado com sucesso!\"); });</script>";
		}
		else if($_GET['msg_success'] == 'deleta_categoria'){
			$titulo_get = anti_injection($_GET['titulo']);
			echo "<script>$(function(){ $(\".alert_success font\").html(\"Categoria do Portfólio <span class='text-semibold'>" . $titulo_get . "</span> deletada com sucesso!\"); });</script>";
		}
		else if($_GET['msg_success'] == 'cria'){
			echo "<script>$(function(){ $(\".alert_success font\").html(\"Portfólio <span class='text-semibold'>" . $titulo . "</span> criado com sucesso!\"); });</script>";
		}
	}

?>

<body class="navbar-top">
	<?php
		// Inseri a barra fixa do topo
		include("includes/navbar-top.php");
	?>
	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">
			<?php
				// Inseri a barra fixa do topo
				include("includes/sidebar.php");
			?>	
			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Page header -->
				<div class="page-header">
					<div class="page-header-content">
						<div class="page-title">
							<h4><span class="text-semibold">Portfólio</span></h4>
						</div>

						<div class="heading-elements">
							<div class="heading-btn-group">
								<a href="portfolio_novo.php"><button type="button" class="btn btn-labeled bg-teal-300"><b><i class="icon-plus2"></i></b> Novo Portfólio</button></a>
							</div>
						</div>
					</div>

				</div>
				<!-- /page header -->


				<!-- Content area -->
				<div class="content">

					<div class="row">
						<div class="col-sm-12">
							<div class="alert  alert-bordered alert-success hide alert_success">
								<button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Fechar</span></button>
								<font></font> 
						  	</div>

							<div class="panel">
								<div class="panel-body">
									<h4 class="text-light">Portfólios Cadastrados</h4>
								</div>
								<?

									$query_portfolio = "SELECT * FROM portfolio ORDER BY codigo DESC";
									$result_portfolio = mysql_query($query_portfolio) or die(mysql_error());
									$row_portfolio = mysql_num_rows($result_portfolio);

									if($row_portfolio > 0){
								?>
								<table class="table datatable-show-all">
									<thead>
										<tr>
											<th style="width: 13% !important;">Capa</th>
											<th>Título</th>
											<th>Tipo</th>
											<th>Categorias</th>
											<th class="text-center">Visualizações</th>
											<th class="text-center">Curtidas</th>
											<th class="text-center">Ação</th>
										</tr>
									</thead>
									<tbody>
										<?php
											while($vet_portfolio = mysql_fetch_array($result_portfolio)){

												$codigo_portfolio = $vet_portfolio['codigo'];
										?>
											<tr>
												<td style="width: 13% !important;">
													<?
														if($vet_portfolio['tipo_capa'] == 1){
													?>
														<div class="bg-capa" style="background-image: url('../img/portfolio/<?=$vet_portfolio['img_capa'];?>');"></div>
													<?
														}
														elseif($vet_portfolio['tipo_capa'] == 2){
													?>
														<div class="img-capa">
															<img src="../img/portfolio/<?=$vet_portfolio['img_capa'];?>" />
														</div>
													<?
														}
													?>
												</td>
												<td>
													<b><?php echo $vet_portfolio['titulo']; ?></b><br>
												</td>
												<td>
													<?
														$query_tipos = "SELECT * FROM portfolio_tipos WHERE codigo_portfolio = '$codigo_portfolio'";
														$result_tipos = mysql_query($query_tipos) or die(mysql_error());
														while($vet_tipo = mysql_fetch_array($result_tipos)){
															if($vet_tipo['tipo'] == 1){ // Filme (Amarelo - #f7bd2b)
																echo "<span class=\"label label-sm label-rounded label-icon\" style=\"background-color: #f7bd2b; border-color: #f7bd2b; margin: 0 3px;\"><i class=\"icon-video-camera2\"></i></span>";
															}
															if($vet_tipo['tipo'] == 2){ // Design (Vermelho - #de294c)
																echo "<span class=\"label label-sm label-rounded label-icon\" style=\"background-color: #de294c; border-color: #de294c; margin: 0 3px;\"><i class=\"icon-pencil-ruler\"></i></span>";
															}
															if($vet_tipo['tipo'] == 3){ // Música (Azul - #009c9d)
																echo "<span class=\"label label-sm label-rounded label-icon\" style=\"background-color: #009c9d; border-color: #009c9d; margin: 0 3px;\"><i class=\"icon-music\"></i></span>";
															}
														}
													?>
												</td>
												<td>
													<?
														$query_categorias = "SELECT * FROM portfolio_categorias WHERE codigo_portfolio = '$codigo_portfolio'";
														$result_categorias = mysql_query($query_categorias) or die(mysql_error());
														while($vet_categoria = mysql_fetch_array($result_categorias)){
													?>
														<div class="btn-group">
							                                <button type="button" class="btn bg-teal btn-xs dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><?=$vet_categoria['titulo_categoria'];?> <span class="caret"></span></button>
															<ul class="dropdown-menu">
																<li><a href="javascript:;" onclick="editarCategoria('<?=$codigo_portfolio;?>', '<?=$vet_categoria['codigo'];?>', '<?=$vet_categoria['titulo_categoria'];?>', '<?=$vet_categoria['titulo_categoria_en'];?>');"><i class="icon-pencil4"></i> Editar Título</a></li>
																<li><a href="portfolio_conteudo.php?portfolio=<?=$codigo_portfolio;?>&categoria=<?=$vet_categoria['codigo'];?>"><i class="icon-pencil3"></i> Editar Conteúdo</a></li>
																<li><a href="javascript:;" onclick="confirmarExclusao('categoria', '<?=$codigo_portfolio;?>','<?=$vet_categoria['codigo'];?>', '<?=$vet_categoria['titulo_categoria'];?>');"><i class="icon-cancel-circle2"></i> Excluir</a></li>
															</ul>
							                            </div>
													<?
														}
													?>
													<a href="javascript:;" onclick="openNovaCategoria('<?=$codigo_portfolio;?>', '<?php echo $vet_portfolio['titulo']; ?>');" class="btn btn-xs bg-teal-300"><i class="icon-plus2"></i></a>
												</td>
												<td class="text-center">
													<?php echo $vet_portfolio['visualizacoes']; ?>
												</td>
												<td class="text-center">
													<?
														$query_curtidas = "SELECT * FROM portfolio_curtidas WHERE codigo_portfolio = '$codigo_portfolio'";
														$result_curtidas = mysql_query($query_curtidas) or die(mysql_error());
														$qntd_curtidas = mysql_num_rows($result_curtidas);

														echo $qntd_curtidas;
													?>
												</td>
												<td class="text-center">
													<ul class="icons-list">
														<li class="dropdown">
															<a href="#" class="dropdown-toggle" data-toggle="dropdown">
																<i class="icon-menu9"></i>
															</a>
															<ul class="dropdown-menu dropdown-menu-right">
																<li><a href="portfolio_edit.php?edit=<?php echo $vet_portfolio['codigo']; ?>"><i class="icon-pencil3"></i> EDITAR</a></li>
																<li><a href="javascript:;" onclick="confirmarExclusao('portfolio', '<?php echo $vet_portfolio['codigo']; ?>', '', '<?php echo $vet_portfolio['titulo']; ?>');"><i class="icon-cancel-circle2"></i> DELETAR</a></li>
															</ul>
														</li>
													</ul>
												</td>
											</tr>
										<?
											}
										?>
									</tbody>
								</table>
								<?
									}
									else{
								?> 
									<div class="row" style="margin-left:10px;">
										<div class="col-lg-4 col-md-6">
											<div class="alert alert-bordered alert-danger">
												Nenhuma portfólio cadastrado no momento.
									  		</div>
									  	</div>
									</div>
							  	<?
								  }
							  	?>
							</div>
						</div>
					</div>	

				</div>

				<script>

					function confirmarExclusao(tipo, portfolio, categoria, titulo){
						if(tipo == "portfolio"){
							text_tipo = "o portfólio";
							link = "portfolio.php?delet=" + portfolio;
						}
						if(tipo == "categoria"){
							text_tipo = "a categoria";
							link = "portfolio.php?delet_categoria=" + categoria + "&portfolio=" + portfolio;
						}
				        bootbox.confirm({
    						size: "small",
				        	message: "Ao apagar " + text_tipo + " <b>" + titulo + "</b>, você irá apagar todo o seu conteúdo.",
				        	buttons: {
						        confirm: {
						            label: 'APAGAR',
						            className: 'btn-danger'
						        },
						        cancel: {
						            label: 'CANCELAR',
						            className: 'btn-default'
						        }
						    },
				        	callback: function(result) {
				            	if(result == true){
				            		window.location = link;
				            	}
				            }
				        });
					}

					function openNovaCategoria(codigo, titulo){
						$("#modal_nova_categoria h5 b").append(titulo);
						$("#modal_nova_categoria input#input_codigo_portfolio").val(codigo);

						$("#modal_nova_categoria").modal('show');
					}

					function editarCategoria(codigo_portfolio, codigo_categoria, titulo, titulo_en){
						$("#modal_editar_categoria h5 b").append(titulo);
						$("#modal_editar_categoria input#input_codigo_portfolio").val(codigo_portfolio);
						$("#modal_editar_categoria input#input_codigo_categoria").val(codigo_categoria);
						$("#modal_editar_categoria input#input_titulo_categoria").val(titulo);
						$("#modal_editar_categoria input#input_titulo_categoria_en").val(titulo_en);

						$("#modal_editar_categoria").modal('show');
					}

					$(function(){

						$(".bg-capa").css("height", $(".bg-capa").width());
						$(".img-capa").css("height", $(".img-capa").width());

					})

				</script>

				<!-- Horizontal form modal -->
				<div id="modal_nova_categoria" class="modal fade">
					<div class="modal-dialog modal-md">
						<div class="modal-content">
							<div class="modal-header text-center">
								<button type="button" class="close" data-dismiss="modal">&times;</button>
								<h5 class="modal-title">Adicione uma Nova Categoria em <b></b></h5>
							</div>

							<form action="portfolio.php" method="post" class="form-horizontal">
								<input type="hidden" name="cmd" value="nova_categoria" />
								<input type="hidden" name="codigo_portfolio" id="input_codigo_portfolio" value="" />
								<div class="modal-body">
									<div class="form-group">
										<label class="control-label col-sm-4 text-right"><img src="assets/images/baneira-pt.png" width="20px" style="position: relative; top: -1px;" /> Título da Categoria:</label>
										<div class="col-sm-8">
											<input type="text" placeholder="" name="titulo_categoria" class="form-control" required />
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-sm-4 text-right"><img src="assets/images/baneira-en.png" width="20px" style="position: relative; top: -1px;" /> Título da Categoria:</label>
										<div class="col-sm-8">
											<input type="text" placeholder="" name="titulo_categoria_en" class="form-control" />
										</div>
									</div>
								</div>

								<div class="modal-footer">
									<button type="button" class="btn btn-default" data-dismiss="modal">CANCELAR</button>
									<button type="submit" class="btn bg-teal">CRIAR</button>
								</div>
							</form>
						</div>
					</div>
				</div>
				<!-- /horizontal form modal -->

				<!-- Horizontal form modal -->
				<div id="modal_editar_categoria" class="modal fade">
					<div class="modal-dialog modal-md">
						<div class="modal-content">
							<div class="modal-header text-center">
								<button type="button" class="close" data-dismiss="modal">&times;</button>
								<h5 class="modal-title">Edite a Categoria <b></b></h5>
							</div>

							<form action="portfolio.php" method="post" class="form-horizontal">
								<input type="hidden" name="cmd" value="edit_categoria" />
								<input type="hidden" name="codigo_portfolio" id="input_codigo_portfolio" value="" />
								<input type="hidden" name="codigo_categoria" id="input_codigo_categoria" value="" />
								<div class="modal-body">
									<div class="form-group">
										<label class="control-label col-sm-3 text-right"><img src="assets/images/baneira-pt.png" width="20px" style="position: relative; top: -1px;" /> Título:</label>
										<div class="col-sm-9">
											<input type="text" placeholder="" name="titulo_categoria" id="input_titulo_categoria" class="form-control" required />
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-sm-3 text-right"><img src="assets/images/baneira-en.png" width="20px" style="position: relative; top: -1px;" /> Título:</label>
										<div class="col-sm-9">
											<input type="text" placeholder="" name="titulo_categoria_en" id="input_titulo_categoria_en" class="form-control" />
										</div>
									</div>
								</div>

								<div class="modal-footer">
									<button type="button" class="btn btn-default" data-dismiss="modal">CANCELAR</button>
									<button type="submit" class="btn bg-teal">ALTERAR</button>
								</div>
							</form>
						</div>
					</div>
				</div>
				<!-- /horizontal form modal -->


<?php
	include("includes/footer.php");
?>