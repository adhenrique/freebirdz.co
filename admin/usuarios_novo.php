<?php
	session_start();

	$include_js = '
	<script type="text/javascript" src="assets/js/plugins/media/fancybox.min.js"></script>

	<script type="text/javascript" src="assets/js/plugins/forms/selects/select2.min.js"></script>
	<script type="text/javascript" src="assets/js/plugins/forms/styling/uniform.min.js"></script>

	<script type="text/javascript" src="assets/js/pages/user_pages_team.js"></script>
	<script type="text/javascript" src="assets/js/pages/form_layouts.js"></script>

	<script type="text/javascript">
		$(function(){
			$("#link_sidebar_usuarios").addClass("active");
		});
	</script>';
	$include_ccs = "";
	include("includes/header.php");
	include("includes/verifica.php");

	if($_SESSION['admin'] == 0){
		redireciona('dashboard.php');
		die;
	}

	if($_POST['cmd'] == "novo"){

		$nome = addslashes($_POST['nome']);
		$sobrenome = addslashes($_POST['sobrenome']);
		$email = addslashes($_POST['email']);
		$login = addslashes($_POST['usuario']);
		$status = $_POST['status'];
		$admin = $_POST['admin'];

		$senha = $_POST['senha'];
		$senha_repeat = $_POST['senha_repeat'];

		$img_perfil = $_FILES['foto_perfil'];

		if($senha){
			if($senha == $senha_repeat){
				$senha = md5($senha);

				if($img_perfil['tmp_name']){
					$diretorio = "uploads/perfil/";
					$extensao = explode("/", $img_perfil["type"]);
					$novo_nome = $nome . "-" . mt_rand(0,9) . "-" . date('d-m-Y-H-i-s') . "-" . mt_rand(1111,9999) . "." . $extensao[1];
					if(move_uploaded_file($img_perfil["tmp_name"], $diretorio . $novo_nome)){

						$query_insert_image = "INSERT INTO usuarios (codigo, nome, sobrenome, email, img_perfil, login, senha, data_criacao, status, admin) VALUES (0, '$nome', '$sobrenome', '$email', '$novo_nome', '$login', '$senha', '$data_hora_eua', '$status', '$admin')";
						$result_insert_image = mysql_query($query_insert_image) or die(mysql_error());
						$id_insert_image = mysql_insert_id();

						redireciona('usuarios.php?msg_success=cria&codigo=' . $id_insert_image);

					}
				}
				else{
					$query_insert_user = "INSERT INTO usuarios (codigo, nome, sobrenome, email, img_perfil, login, senha, data_criacao, status, admin) VALUES (0, '$nome', '$sobrenome', '$email', '', '$login', '$senha', '$data_hora_eua', '$status', '$admin')";
					$result_insert_user = mysql_query($query_insert_user) or die(mysql_error());
					$id_insert_user = mysql_insert_id();

					redireciona('usuarios.php?msg_success=cria&codigo=' . $id_insert_user);
				}
			}
		}

	}
?>

<body class="navbar-top">
	<?php
		// Inseri a barra fixa do topo
		include("includes/navbar-top.php");
	?>
	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">
			<?php
				// Inseri a barra fixa do topo
				include("includes/sidebar.php");
			?>	
			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Page header -->
				<div class="page-header">
					<div class="page-header-content">
						<div class="page-title">
							<h4><span class="text-semibold">Usuários</span> - Novo Usuário</h4>
						</div>
					</div>
					<div class="breadcrumb-line">
						<ul class="breadcrumb">
							<li><a href="usuarios.php"><i class="icon-users4 position-left"></i> Usuários</a></li>
							<li class="active">Novo Usuário</li>
						</ul>
					</div>

				</div>
				<!-- /page header -->


				<!-- Content area -->
				<div class="content">

					<div class="row">
						<div class="col-sm-12">
							<div class="panel">
								<form method="post" class="form-horizontal" enctype="multipart/form-data">
									<input type="hidden" name="cmd" value="novo" />
									<div class="panel-body">
										<div class="col-sm-6">
											<div class="form-group">
												<label class="col-lg-3 control-label">Nome:</label>
												<div class="col-lg-9">
													<input type="text" name="nome" class="form-control" placeholder="">
												</div>
											</div>
										</div>
										<div class="col-sm-6">
											<div class="form-group">
												<label class="col-lg-3 control-label">Sobrenome:</label>
												<div class="col-lg-9">
													<input type="text" name="sobrenome" class="form-control" placeholder="">
												</div>
											</div>
										</div>
										<div class="col-sm-6">
											<div class="form-group">
												<label class="col-lg-3 control-label">Usúario:</label>
												<div class="col-lg-9">
													<input type="text" name="usuario" class="form-control" placeholder="">
												</div>
											</div>
										</div>
										<div class="col-sm-6">
											<div class="form-group">
												<label class="col-lg-3 control-label">E-mail:</label>
												<div class="col-lg-9">
													<input type="email" name="email" class="form-control" placeholder="">
												</div>
											</div>
										</div>
										<div class="col-sm-6">
											<div class="form-group">
												<label class="col-lg-3 control-label">Nova Senha:</label>
												<div class="col-lg-9">
													<input type="password" name="senha" class="form-control" placeholder="">
												</div>
											</div>
										</div>
										<div class="col-sm-6">
											<div class="form-group">
												<label class="col-lg-3 control-label">Repita a Senha:</label>
												<div class="col-lg-9">
													<input type="password" name="senha_repeat" class="form-control" placeholder="">
												</div>
											</div>
										</div>
										<div class="col-sm-6">
											<div class="form-group">
												<label class="col-lg-3 control-label">Foto de Perfil:</label>
												<div class="col-lg-9">
													<div class="media no-margin-top">
														<div class="media-left">
															<img src="assets/images/user_default.jpg" style="width: 58px; height: 58px;" class="img-rounded" alt="">
														</div>

														<div class="media-body">
															<input type="file" name="foto_perfil" class="file-styled">
															<span class="help-block">Formatos suportados: gif, png, jpg. Tamanho máximo 2Mb</span>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="col-sm-6">
											<div class="form-group" id="radio_status">
												<label class="col-lg-3 control-label">Status:</label>
												<div class="col-lg-9">
													<label class="radio-inline">
														<input type="radio" class="styled" name="status" value="1" checked="checked">
														Ativo
													</label>

													<label class="radio-inline">
														<input type="radio" class="styled" name="status" value="0">
														Inativo
													</label>
												</div>
											</div>
										</div>
										<div class="col-sm-6">
											<div class="form-group" id="radio_status">
												<label class="col-lg-3 control-label">Administrador:</label>
												<div class="col-lg-9">
													<label class="radio-inline">
														<input type="radio" class="styled" name="admin" value="1" checked="checked">
														Sim
													</label>

													<label class="radio-inline">
														<input type="radio" class="styled" name="admin" value="0">
														Não
													</label>
												</div>
											</div>
										</div>

										<div class="col-sm-12">
											<div class="text-right">
												<button type="submit" class="btn bg-teal-300">Criar <i class="icon-arrow-right14 position-right"></i></button>
											</div>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>	

					</div>
					<!-- /dropdown menu -->
<?php
	include("includes/footer.php");
?>