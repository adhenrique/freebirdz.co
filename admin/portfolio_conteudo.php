<?php
	session_start();

	$include_js = '
	<script type="text/javascript" src="assets/js/plugins/media/fancybox.min.js"></script>

	<script type="text/javascript" src="assets/js/plugins/forms/selects/select2.min.js"></script>
	<script type="text/javascript" src="assets/js/plugins/forms/styling/uniform.min.js"></script>

	<script type="text/javascript" src="assets/js/pages/form_layouts.js"></script>

	<script type="text/javascript" src="assets/js/plugins/notifications/bootbox.min.js"></script>

	<script type="text/javascript" src="assets/js/pages/user_pages_team.js"></script>

	<script type="text/javascript">
		$(function(){
			$("#link_sidebar_portfolio").addClass("active");
		});
	</script>';
	$include_css = "";
	include("includes/header.php");
	include("includes/verifica.php");

	if($_POST['cmd'] == "novo_conteudo"){

		$codigo_portfolio = $_POST['codigo_portfolio'];
		$codigo_categoria = $_POST['codigo_categoria'];
		$titulo = $_POST['titulo'];
		$video = $_POST['video'];
		$gif = $_POST['gif'];
		$texto = $_POST['texto'];

		$titulo_en = $_POST['titulo_en'];
		$video_en = $_POST['video_en'];
		$gif_en = $_POST['gif_en'];
		$texto_en = $_POST['texto_en'];

		$imagem = $_FILES['imagem'];
		$imagem_en = $_FILES['imagem_en'];
		
		$query_insert_conteudo = "INSERT INTO portfolio_conteudo (codigo, codigo_portfolio, codigo_categoria, titulo, texto, imagem, video, titulo_en, texto_en, imagem_en, video_en, gif, gif_en) VALUES (0, '$codigo_portfolio', '$codigo_categoria', '$titulo', '$texto', '', '$video', '$titulo_en', '$texto_en', '', '$video_en', '$gif', '$gif_en')";
		$result_insert_conteudo = mysql_query($query_insert_conteudo) or die(mysql_error());
		$id_insert_conteudo = mysql_insert_id();

		$diretorio = "../img/portfolio/";

		if($imagem['tmp_name']){
			$extensao_imagem = explode("/", $imagem["type"]);
			$novo_nome_imagem = "imagem-" . $id_insert_conteudo . "-" . date('d-m-Y-H-i-s') . "-" . mt_rand(1111,9999) . "." . $extensao_imagem[1];
			if(move_uploaded_file($imagem["tmp_name"], $diretorio . $novo_nome_imagem)){
				mysql_query("UPDATE portfolio_conteudo SET imagem = '$novo_nome_imagem' WHERE codigo = '$id_insert_conteudo' LIMIT 1");
			}
		}

		if($imagem_en['tmp_name']){
			$extensao_imagem_en = explode("/", $imagem_en["type"]);
			$novo_nome_imagem_en = "imagem_en-" . $id_insert_conteudo . "-" . date('d-m-Y-H-i-s') . "-" . mt_rand(1111,9999) . "." . $extensao_imagem_en[1];
			if(move_uploaded_file($imagem_en["tmp_name"], $diretorio . $novo_nome_imagem_en)){
				mysql_query("UPDATE portfolio_conteudo SET imagem_en = '$novo_nome_imagem_en' WHERE codigo = '$id_insert_conteudo' LIMIT 1");
			}
		}

        redireciona("portfolio_conteudo.php?categoria={$codigo_categoria}&portfolio={$codigo_portfolio}&msg_success=true");
	}


	if($_POST['cmd'] == "edit_conteudo"){

		$codigo_conteudo = $_POST['codigo_conteudo'];
		$codigo_portfolio = $_POST['codigo_portfolio'];
		$codigo_categoria = $_POST['codigo_categoria'];

		$titulo = $_POST['titulo'];
		$video = $_POST['video'];
		$gif = $_POST['gif'];
		$texto = $_POST['texto'];

		$titulo_en = $_POST['titulo_en'];
		$video_en = $_POST['video_en'];
		$gif_en = $_POST['gif_en'];
		$texto_en = $_POST['texto_en'];

		$imagem = $_FILES['imagem'];
		$imagem_en = $_FILES['imagem_en'];

		mysql_query("UPDATE portfolio_conteudo SET titulo = '$titulo', video = '$video', texto = '$texto', titulo_en = '$titulo_en', video_en = '$video_en', texto_en = '$texto_en', gif = '$gif', gif_en = '$gif_en' WHERE codigo_categoria = '$codigo_categoria' AND codigo_portfolio = '$codigo_portfolio' AND codigo = '$codigo_conteudo' LIMIT 1");

		$query_consulta_conteudo = "SELECT * FROM portfolio_conteudo WHERE codigo_categoria = '$codigo_categoria' AND codigo_portfolio = '$codigo_portfolio' AND codigo = '$codigo_conteudo' LIMIT 1";
		$result_consulta_conteudo = mysql_query($query_consulta_conteudo) or die(mysql_error());
		$vet_consulta_conteudo = mysql_fetch_array($result_consulta_conteudo);

		$diretorio = "../img/portfolio/";

		if($imagem['tmp_name']){
			$extensao_imagem = explode("/", $imagem["type"]);
			$novo_nome_imagem = "imagem-" . $portfolio_edit . "-" . date('d-m-Y-H-i-s') . "-" . mt_rand(1111,9999) . "." . $extensao_imagem[1];
			if(move_uploaded_file($imagem["tmp_name"], $diretorio . $novo_nome_imagem)){
				mysql_query("UPDATE portfolio_conteudo SET imagem = '$novo_nome_imagem' WHERE codigo = '$codigo_conteudo' LIMIT 1");

				if($vet_consulta_conteudo['imagem']){
					unlink($diretorio . $vet_consulta_conteudo['imagem']);
				}
			}
		}

		if($imagem_en['tmp_name']){
			$extensao_imagem_en = explode("/", $imagem_en["type"]);
			$novo_nome_imagem_en = "imagem_en-" . $portfolio_edit . "-" . date('d-m-Y-H-i-s') . "-" . mt_rand(1111,9999) . "." . $extensao_imagem_en[1];
			if(move_uploaded_file($imagem_en["tmp_name"], $diretorio . $novo_nome_imagem_en)){
				mysql_query("UPDATE portfolio_conteudo SET imagem_en = '$novo_nome_imagem_en' WHERE codigo = '$codigo_conteudo' LIMIT 1");

				if($vet_consulta_conteudo['imagem_en']){
					unlink($diretorio . $vet_consulta_conteudo['imagem_en']);
				}
			}
		}

        redireciona("portfolio_conteudo.php?categoria={$codigo_categoria}&portfolio={$codigo_portfolio}&msg_success=true");
	}



	if($_GET['portfolio'] AND $_GET['categoria']){

		$portfolio_edit = anti_injection($_GET['portfolio']);
		$categoria_edit = anti_injection($_GET['categoria']);

		$query_portfolio = "SELECT * FROM portfolio WHERE codigo = '$portfolio_edit' LIMIT 1";
		$result_portfolio = mysql_query($query_portfolio) or die(mysql_error());
		$row_portfolio = mysql_num_rows($result_portfolio);

		if($row_portfolio > 0){

			$vet_portfolio = mysql_fetch_array($result_portfolio);

			$query_categoria = "SELECT * FROM portfolio_categorias WHERE codigo = '$categoria_edit' AND codigo_portfolio = '$portfolio_edit' LIMIT 1";
			$result_categoria = mysql_query($query_categoria) or die(mysql_error());
			$row_categoria = mysql_num_rows($result_categoria);

			if($row_categoria > 0){
				$vet_categoria = mysql_fetch_array($result_categoria);
			}
			else{
				redireciona("portfolio.php");
			}
		}
		else{
			redireciona("portfolio.php");
		}
	}
	else{
		redireciona("portfolio.php");
	}


	if(isset($_GET['delet'])){

		$id_delet_conteudo = $_GET['delet'];
		$id_delet_categoria = $_GET['categoria'];
		$id_delet_portfolio = $_GET['portfolio'];

		$query_consulta_conteudo = "SELECT * FROM portfolio_conteudo WHERE codigo_categoria = '$id_delet_categoria' AND codigo_portfolio = '$id_delet_portfolio' AND codigo = '$id_delet_conteudo' LIMIT 1";
		$result_consulta_conteudo = mysql_query($query_consulta_conteudo) or die(mysql_error());
		$vet_consulta_conteudo = mysql_fetch_array($result_consulta_conteudo);

		if($vet_consulta_conteudo['imagem']){
			unlink($diretorio . $vet_consulta_conteudo['imagem']);
		}
		if($vet_consulta_conteudo['imagem_en']){
			unlink($diretorio . $vet_consulta_conteudo['imagem_en']);
		}

		mysql_query("DELETE FROM portfolio_conteudo WHERE codigo_categoria = '$id_delet_categoria' AND codigo_portfolio = '$id_delet_portfolio' AND codigo = '$id_delet_conteudo' LIMIT 1");

//        redireciona("portfolio_conteudo.php?categoria={$codigo_categoria}&portfolio={$codigo_portfolio}&msg_success=true");
	}


//	if(isset($_GET['msg_success'])){
//		$codigo_portfolio = anti_injection($_GET['codigo']);
//		$query = "SELECT * FROM portfolio WHERE codigo = '$codigo_portfolio'";
//		$result = mysql_query($query) or die(mysql_error());
//		$vet = mysql_fetch_array($result);
//		$titulo = $vet['titulo'];
//
//		echo "<script>$(function(){ $(\".alert_success\").removeClass(\"hide\"); });</script>";
//
//		if($_GET['msg_success'] == 'deleta'){
//			$titulo_get = anti_injection($_GET['titulo']);
//			echo "<script>$(function(){ $(\".alert_success font\").html(\"Portfólio <span class='text-semibold'>" . $titulo_get . "</span> deletado com sucesso!\"); });</script>";
//		}
//		else if($_GET['msg_success'] == 'cria'){
//			echo "<script>$(function(){ $(\".alert_success font\").html(\"Portfólio <span class='text-semibold'>" . $titulo . "</span> criado com sucesso!\"); });</script>";
//		}
//	}

?>

<body class="navbar-top">
	<?php
		// Inseri a barra fixa do topo
		include("includes/navbar-top.php");
	?>
	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">
			<?php
				// Inseri a barra fixa do topo
				include("includes/sidebar.php");
			?>
			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Page header -->
				<div class="page-header">
					<div class="page-header-content">
						<div class="page-title">
							<h4><span class="text-semibold">Conteúdo Categoria (<b><?=$vet_categoria['titulo_categoria'];?></b>) do Portfólio (<b><?=$vet_portfolio['titulo'];?></b>)</span></h4>
						</div>

						<div class="heading-elements">
							<div class="heading-btn-group">
								<a href="javascript:;" onclick="openNovoConteudo();"><button type="button" class="btn btn-labeled bg-teal-300"><b><i class="icon-plus2"></i></b> Novo Bloco de Conteúdo</button></a>
							</div>
						</div>
					</div>

				</div>
				<!-- /page header -->


				<!-- Content area -->
				<div class="content">

					<div class="row">
						<div class="col-sm-12">
                            <?php if ($_GET['msg_success']): ?>
                                <div class="alert  alert-bordered alert-success alert_success" id="msg_success">
                                    <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Fechar</span></button>
                                    Alterações no conteúdo realizadas com sucesso!
                                </div>
                            <?php endif; ?>
                            <?php if ($_GET['msg_error']): ?>
                                <div class="alert  alert-bordered alert-danger alert_error" id="msg_error">
                                    <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Fechar</span></button>
                                    Você precisa enviar ao menos uma imagem ou vídeo.
                                </div>
                            <?php endif; ?>

							<div class="panel">
								<div class="panel-body">
									<h4 class="text-light">Conteúdos Cadastrados</h4>
								</div>
								<?

									$query_conteudo = "SELECT * FROM portfolio_conteudo WHERE codigo_categoria = '$categoria_edit' AND codigo_portfolio = '$portfolio_edit' ORDER BY codigo DESC";
									$result_conteudo = mysql_query($query_conteudo) or die(mysql_error());
									$row_conteudo = mysql_num_rows($result_conteudo);

									if($row_conteudo > 0){
								?>
								<table class="table datatable-show-all">
									<thead>
										<tr>
											<th style="width: 15% !important;">Imagem/Vídeo</th>
											<th>Título</th>
											<th class="text-center">Ação</th>
										</tr>
									</thead>
									<tbody>
										<?php
											while($vet_conteudo = mysql_fetch_array($result_conteudo)){

												$codigo_conteudo = $vet_conteudo['codigo'];
												$codigo_conteudo_portfolio = $vet_conteudo['codigo_portfolio'];
												$codigo_conteudo_categoria = $vet_conteudo['codigo_categoria'];
										?>
											<tr>
												<td style="width: 15% !important;">
													<?
														if($vet_conteudo['imagem']){
													?>
														<div class="thumb">
															<div class="img-bg" style="background-image: url('../img/portfolio/<?=$vet_conteudo['imagem'];?>'); background-repeat: no-repeat; background-size: cover; background-position: center center;">
															</div>
															<div class="caption-overflow">
																<span>
																	<a href="../img/portfolio/<?=$vet_conteudo['imagem'];?>" class="btn bg-teal btn-icon btn-xs" data-popup="lightbox"><i class="icon-image2"></i></a>
																</span>
															</div>
														</div>
													<?
														}
														else{
															if($vet_conteudo['video']){
																if(strpos($vet_conteudo['video'], "youtube")){
																	$url = $vet_conteudo['video'];
																	preg_match('/[\\?\\&]v=([^\\?\\&]+)/', $url, $matches);
																	$codigo_youtube = $matches[1];
															?>
																	<div class="thumb">
																		<div class="img-bg" style="background-image: url('<?=thumbnailYouTube($codigo_youtube);?>'); background-repeat: no-repeat; background-size: cover; background-position: center center;">
																		</div>
																		<div class="caption-overflow">
																			<span>
																				<a href="https://www.youtube.com/watch?v=<?=$codigo_youtube;?>" class="btn bg-teal btn-icon btn-xs" target="_blank"><i class="icon-play"></i></a>
																			</span>
																		</div>
																	</div>
															<?
																}
																if(strpos($vet_conteudo['video'], "vimeo")){
																	$url = $vet_conteudo['video'];
																	preg_match('%^https?:\/\/(?:www\.|player\.)?vimeo.com\/(?:channels\/(?:\w+\/)?|groups\/([^\/]*)\/videos\/|album\/(\d+)\/video\/|video\/|)(\d+)(?:$|\/|\?)(?:[?]?.*)$%im', $url ,$matches);
																	$codigo_vimeo = $matches[3];
															?>
																	<div class="thumb">
																		<div class="img-bg" style="background-image: url('<?=thumbnailVimeo($codigo_vimeo);?>'); background-repeat: no-repeat; background-size: cover; background-position: center center;">
																		</div>
																		<div class="caption-overflow">
																			<span>
																				<a href="https://vimeo.com/<?=$codigo_vimeo;?>" class="btn bg-teal btn-icon btn-xs" target="_blank"><i class="icon-play"></i></a>
																			</span>
																		</div>
																	</div>
															<?
																}
															}
														}
													?>
												</td>
												<td>
													<b><?php echo $vet_conteudo['titulo']; ?></b><br>
												</td>
												<td class="text-center">
													<ul class="icons-list">
														<li class="dropdown">
															<a href="#" class="dropdown-toggle" data-toggle="dropdown">
																<i class="icon-menu9"></i>
															</a>
															<ul class="dropdown-menu dropdown-menu-right">
																<li><a href="javascript:;" onclick="editarConteudo('<?=$codigo_conteudo_portfolio;?>', '<?=$codigo_conteudo_categoria;?>', '<?=$codigo_conteudo;?>', '<?=$vet_conteudo['titulo'];?>', '<?=$vet_conteudo['video'];?>', '<?=$vet_conteudo['imagem'];?>', '<?=$vet_conteudo['texto'];?>', '<?=$vet_conteudo['titulo_en'];?>', '<?=$vet_conteudo['video_en'];?>', '<?=$vet_conteudo['imagem_en'];?>', '<?=$vet_conteudo['texto_en'];?>');"><i class="icon-pencil3"></i> EDITAR</a></li>
																<li><a href="javascript:;" onclick="confirmarExclusao('<?=$codigo_conteudo_portfolio;?>', '<?=$codigo_conteudo_categoria;?>', '<?=$codigo_conteudo;?>', '<?=$vet_conteudo['titulo'];?>');"><i class="icon-cancel-circle2"></i> DELETAR</a></li>
															</ul>
														</li>
													</ul>
												</td>
											</tr>
										<?
											}
										?>
									</tbody>
								</table>
								<?
									}
									else{
								?>
									<div class="row" style="margin-left:10px;">
										<div class="col-lg-4 col-md-6">
											<div class="alert alert-bordered alert-danger">
												Nenhum conteúdo cadastrado no momento.
									  		</div>
									  	</div>
									</div>
							  	<?
								  }
							  	?>
							</div>
						</div>
					</div>

				</div>

				<script>

					function confirmarExclusao(portfolio, categoria, conteudo, titulo){
				        bootbox.confirm({
    						size: "small",
				        	message: "Tem certeza que deseja apagar o conteúdo <b>" + titulo + "</b>?",
				        	buttons: {
						        confirm: {
						            label: 'APAGAR',
						            className: 'btn-danger'
						        },
						        cancel: {
						            label: 'CANCELAR',
						            className: 'btn-default'
						        }
						    },
				        	callback: function(result) {
				            	if(result == true){
				            		window.location = "portfolio_conteudo.php?delet=" + conteudo + "&categoria=" + categoria + "&portfolio=" + portfolio;
				            	}
				            }
				        });
					}

					function openNovoConteudo(){
						$("#modal_novo_conteudo").modal('show');
					}

					function editarConteudo(codigo_portfolio, codigo_categoria, codigo_conteudo, titulo, video, imagem, texto, titulo_en, video_en, imagem_en, texto_en){
						$("#modal_editar_conteudo h5 b").append(titulo);
						$("#modal_editar_conteudo input#input_codigo_portfolio").val(codigo_portfolio);
						$("#modal_editar_conteudo input#input_codigo_categoria").val(codigo_categoria);
						$("#modal_editar_conteudo input#input_codigo_conteudo").val(codigo_conteudo);
						$("#modal_editar_conteudo input#input_edit_titulo").val(titulo);
						$("#modal_editar_conteudo input#input_edit_video").val(video);
						$("#modal_editar_conteudo input#input_edit_titulo").val(titulo);

						$("#modal_editar_conteudo textarea#input_edit_texto").html(texto);
						$("#modal_editar_conteudo input#input_edit_titulo_en").val(titulo_en);
						$("#modal_editar_conteudo input#input_edit_video_en").val(video_en);
						$("#modal_editar_conteudo input#input_edit_titulo_en").val(titulo_en);
						$("#modal_editar_conteudo textarea#input_edit_texto_en").html(texto_en);

						if(imagem){
							$("#modal_editar_conteudo .media img#input_edit_imagem").attr("src", "../img/portfolio/" + imagem);
						}
						else{
							$("#modal_editar_conteudo .media img#input_edit_imagem").attr("src", "assets/images/img_default.jpg");
						}

						if(imagem_en){
							$("#modal_editar_conteudo .media img#input_edit_imagem_en").attr("src", "../img/portfolio/" + imagem_en);
						}
						else{
							$("#modal_editar_conteudo .media img#input_edit_imagem_en").attr("src", "assets/images/img_default.jpg");
						}

						$("#modal_editar_conteudo").modal('show');
					}


					function alturaBG(item, prop){
						tamanhoImagemBG = $(item).width();
						$(item).css("height", tamanhoImagemBG/prop);
					}

					$(function(){

						alturaBG(".content .thumb .img-bg", 1.3);
						$(".bg-capa").css("height", $(".bg-capa").width());
						$(".img-capa").css("height", $(".img-capa").width());

					});

					$(window).resize(function(){
						alturaBG(".content .thumb .img-bg", 1.3);
					});

				</script>

				<!-- Horizontal form modal -->
				<div id="modal_novo_conteudo" class="modal fade">
					<div class="modal-dialog modal-md">
						<div class="modal-content">
							<div class="modal-header text-center">
								<button type="button" class="close" data-dismiss="modal">&times;</button>
								<h5 class="modal-title">Adicione um Novo Conteúdo em <b><?=$vet_categoria['titulo_categoria'];?></b></h5>
							</div>

							<form action="" method="post" class="form-horizontal" enctype="multipart/form-data">
								<input type="hidden" name="cmd" value="novo_conteudo" />
								<input type="hidden" name="codigo_portfolio" value="<?=$portfolio_edit;?>" />
								<input type="hidden" name="codigo_categoria" value="<?=$categoria_edit;?>" />
								<div class="modal-body">
									<div class="form-group">
										<label class="control-label col-sm-3 text-right"><img src="assets/images/baneira-pt.png" width="20px" style="position: relative; top: -1px;" /> Título:</label>
										<div class="col-sm-9">
											<input type="text" placeholder="" name="titulo" class="form-control" />
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-sm-3 text-right"><img src="assets/images/baneira-en.png" width="20px" style="position: relative; top: -1px;" /> Título:</label>
										<div class="col-sm-9">
											<input type="text" placeholder="" name="titulo_en" class="form-control" />
										</div>
									</div>
									<br>
									<div class="form-group">
										<label class="control-label col-sm-3 text-right"><img src="assets/images/baneira-pt.png" width="20px" style="position: relative; top: -1px;" /> Link Vídeo:</label>
										<div class="col-sm-9">
											<input type="text" placeholder="" name="video" class="form-control" />
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-sm-3 text-right"><img src="assets/images/baneira-en.png" width="20px" style="position: relative; top: -1px;" /> Link Vídeo:</label>
										<div class="col-sm-9">
											<input type="text" placeholder="" name="video_en" class="form-control" />
										</div>
									</div>
									<br>
                                    <div class="form-group">
                                        <label class="control-label col-sm-3 text-right"><img src="assets/images/baneira-pt.png" width="20px" style="position: relative; top: -1px;" /> Link Gif:</label>
                                        <div class="col-sm-9">
                                            <input type="text" placeholder="" name="gif" class="form-control" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-3 text-right"><img src="assets/images/baneira-en.png" width="20px" style="position: relative; top: -1px;" /> Link Gif:</label>
                                        <div class="col-sm-9">
                                            <input type="text" placeholder="" name="gif_en" class="form-control" />
                                        </div>
                                    </div>
                                    <br>
									<div class="form-group">
										<label class="col-lg-3 control-label text-right"><img src="assets/images/baneira-pt.png" width="20px" style="position: relative; top: -1px;" /> Imagem:</label>
										<div class="col-lg-9">
											<div class="media no-margin-top">
												<div class="media-left">
													<img src="assets/images/img_default.jpg" style="width: 58px; height: 58px;" class="img-rounded" alt="">
												</div>
												<div class="media-body">
													<input type="file" name="imagem" class="file-styled" />
													<span class="help-block">Formatos suportados: png e jpg.<br>Tamanho máximo recomendado 1.5Mb</span>
												</div>
											</div>
										</div>
									</div>
									<div class="form-group">
										<label class="col-lg-3 control-label text-right"><img src="assets/images/baneira-en.png" width="20px" style="position: relative; top: -1px;" /> Imagem:</label>
										<div class="col-lg-9">
											<div class="media no-margin-top">
												<div class="media-left">
													<img src="assets/images/img_default.jpg" style="width: 58px; height: 58px;" class="img-rounded" alt="">
												</div>
												<div class="media-body">
													<input type="file" name="imagem_en" class="file-styled" />
													<span class="help-block">Formatos suportados: png e jpg.<br>Tamanho máximo recomendado 1.5Mb</span>
												</div>
											</div>
										</div>
									</div>
									<br>
									<div class="form-group">
										<label class="control-label col-sm-3 text-right"><img src="assets/images/baneira-pt.png" width="20px" style="position: relative; top: -1px;" /> Texto:</label>
										<div class="col-sm-9">
											<textarea placeholder="" name="texto" style="resize: none;" rows="5" class="form-control"></textarea>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-sm-3 text-right"><img src="assets/images/baneira-en.png" width="20px" style="position: relative; top: -1px;" /> Texto:</label>
										<div class="col-sm-9">
											<textarea placeholder="" name="texto_en" style="resize: none;" rows="5" class="form-control"></textarea>
										</div>
									</div>
								</div>

								<div class="modal-footer">
									<button type="button" class="btn btn-default" data-dismiss="modal">CANCELAR</button>
									<button type="submit" class="btn bg-teal">CRIAR</button>
								</div>
							</form>
						</div>
					</div>
				</div>
				<!-- /horizontal form modal -->

				<!-- Horizontal form modal -->
				<div id="modal_editar_conteudo" class="modal fade">
					<div class="modal-dialog modal-md">
						<div class="modal-content">
							<div class="modal-header text-center">
								<button type="button" class="close" data-dismiss="modal">&times;</button>
								<h5 class="modal-title">Edite o Conteúdo <b></b></h5>
							</div>

							<form method="post" class="form-horizontal" enctype="multipart/form-data">
								<input type="hidden" name="cmd" value="edit_conteudo" />
								<input type="hidden" id="input_codigo_portfolio" name="codigo_portfolio" value="" />
								<input type="hidden" id="input_codigo_categoria" name="codigo_categoria" value="" />
								<input type="hidden" id="input_codigo_conteudo" name="codigo_conteudo" value="" />
								<div class="modal-body">
									<div class="form-group">
										<label class="control-label col-sm-2 text-right"><img src="assets/images/baneira-pt.png" width="20px" style="position: relative; top: -1px;" /> Título:</label>
										<div class="col-sm-10">
											<input type="text" placeholder="" id="input_edit_titulo" name="titulo" class="form-control" />
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-sm-2 text-right"><img src="assets/images/baneira-en.png" width="20px" style="position: relative; top: -1px;" /> Título:</label>
										<div class="col-sm-10">
											<input type="text" placeholder="" id="input_edit_titulo_en" name="titulo_en" class="form-control" />
										</div>
									</div>
									<br>
									<div class="form-group">
										<label class="control-label col-sm-2 text-right"><img src="assets/images/baneira-pt.png" width="20px" style="position: relative; top: -1px;" /> Link Vídeo:</label>
										<div class="col-sm-10">
											<input type="text" placeholder="" id="input_edit_video" name="video" class="form-control" />
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-sm-2 text-right"><img src="assets/images/baneira-en.png" width="20px" style="position: relative; top: -1px;" /> Link Vídeo:</label>
										<div class="col-sm-10">
											<input type="text" placeholder="" id="input_edit_video_en" name="video_en" class="form-control" />
										</div>
									</div>
									<br>
                                    <div class="form-group">
                                        <label class="control-label col-sm-3 text-right"><img src="assets/images/baneira-pt.png" width="20px" style="position: relative; top: -1px;" /> Link Gif:</label>
                                        <div class="col-sm-9">
                                            <input type="text" placeholder="" name="gif" id="input_edit_gif" class="form-control" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-3 text-right"><img src="assets/images/baneira-en.png" width="20px" style="position: relative; top: -1px;" /> Link Gif:</label>
                                        <div class="col-sm-9">
                                            <input type="text" placeholder="" id="input_edit_gif_en" name="gif_en" class="form-control" />
                                        </div>
                                    </div>
                                    <br>
									<div class="form-group">
										<label class="col-lg-2 control-label text-right"><img src="assets/images/baneira-pt.png" width="20px" style="position: relative; top: -1px;" /> Imagem:</label>
										<div class="col-lg-10">
											<div class="media no-margin-top">
												<div class="media-left">
													<img src="assets/images/img_default.jpg" id="input_edit_imagem" style="width: 58px; height: 58px;" class="img-rounded" alt="">
												</div>
												<div class="media-body">
													<input type="file" name="imagem" class="file-styled" />
													<span class="help-block">Formatos suportados: png e jpg.<br>Tamanho máximo recomendado 1.5Mb</span>
												</div>
											</div>
										</div>
									</div>
									<div class="form-group">
										<label class="col-lg-2 control-label text-right"><img src="assets/images/baneira-en.png" width="20px" style="position: relative; top: -1px;" /> Imagem:</label>
										<div class="col-lg-10">
											<div class="media no-margin-top">
												<div class="media-left">
													<img src="assets/images/img_default.jpg" id="input_edit_imagem_en" style="width: 58px; height: 58px;" class="img-rounded" alt="">
												</div>
												<div class="media-body">
													<input type="file" name="imagem_en" class="file-styled" />
													<span class="help-block">Formatos suportados: png e jpg.<br>Tamanho máximo recomendado 1.5Mb</span>
												</div>
											</div>
										</div>
									</div>
									<br>
									<div class="form-group">
										<label class="control-label col-sm-2 text-right"><img src="assets/images/baneira-pt.png" width="20px" style="position: relative; top: -1px;" /> Texto:</label>
										<div class="col-sm-10">
											<textarea placeholder="" id="input_edit_texto" name="texto" style="resize: none;" rows="5" class="form-control"></textarea>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-sm-2 text-right"><img src="assets/images/baneira-en.png" width="20px" style="position: relative; top: -1px;" /> Texto:</label>
										<div class="col-sm-10">
											<textarea placeholder="" id="input_edit_texto_en" name="texto_en" style="resize: none;" rows="5" class="form-control"></textarea>
										</div>
									</div>
								</div>

								<div class="modal-footer">
									<button type="button" class="btn btn-default" data-dismiss="modal">CANCELAR</button>
									<button type="submit" class="btn bg-teal">EDITAR</button>
								</div>
							</form>
						</div>
					</div>
				</div>
				<!-- /horizontal form modal -->


<?php
	include("includes/footer.php");
?>
