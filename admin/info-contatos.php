<?php
	session_start();

	$include_js = '

	<script type="text/javascript" src="assets/js/plugins/media/fancybox.min.js"></script>
	<script type="text/javascript" src="assets/js/pages/gallery.js"></script>

	<script type="text/javascript" src="assets/js/plugins/forms/selects/select2.min.js"></script>
	<script type="text/javascript" src="assets/js/plugins/forms/styling/uniform.min.js"></script>

	<script type="text/javascript" src="assets/js/pages/form_layouts.js"></script>


	<script type="text/javascript">
		$(function(){
			$("#link_sidebar_info_contatos").addClass("active");
		});
	</script>';

	$include_ccs = "";
	include("includes/header.php");
	include("includes/verifica.php");

	if($_POST['cmd'] == "edit"){

		$titulo_endereco_1 = $_POST['titulo_endereco_1'];
		$endereco_1 = $_POST['endereco_1'];
		$telefone_1 = $_POST['telefone_1'];
		$titulo_endereco_2 = $_POST['titulo_endereco_2'];
		$endereco_2 = $_POST['endereco_2'];
		$telefone_2 = $_POST['telefone_2'];
		$titulo_email_1 = $_POST['titulo_email_1'];
		$email_1 = $_POST['email_1'];
		$titulo_email_2 = $_POST['titulo_email_2'];
		$email_2 = $_POST['email_2'];
		$titulo_email_3 = $_POST['titulo_email_3'];
		$email_3 = $_POST['email_3'];
		$link_facebook = $_POST['link_facebook'];
		$link_instagram = $_POST['link_instagram'];
		$link_twitter = $_POST['link_twitter'];
		$link_vimeo = $_POST['link_vimeo'];
		$link_behance = $_POST['link_behance'];
		$link_pinterest = $_POST['link_pinterest'];
		$link_apple = $_POST['link_apple'];

		$img_informacoes = $_FILES['img_informacoes'];

		$query_alter_dados = "UPDATE contatos SET titulo_endereco_1 = '$titulo_endereco_1', endereco_1 = '$endereco_1', telefone_1 = '$telefone_1', titulo_endereco_2 = '$titulo_endereco_2', endereco_2 = '$endereco_2', telefone_2 = '$telefone_2', titulo_email_1 = '$titulo_email_1', email_1 = '$email_1', titulo_email_2 = '$titulo_email_2', email_2 = '$email_2', titulo_email_3 = '$titulo_email_3', email_3 = '$email_3', link_facebook = '$link_facebook', link_instagram = '$link_instagram', link_twitter = '$link_twitter', link_vimeo = '$link_vimeo', link_behance = '$link_behance', link_pinterest = '$link_pinterest', link_apple = '$link_apple'";
		$result_alter_dados = mysql_query($query_alter_dados) or die(mysql_error());
		

		redireciona("info-contatos.php");
	}


	$query = "SELECT * FROM contatos";
	$result = mysql_query($query) or die (mysql_error());
	$vet_informacoes = mysql_fetch_array($result);
				

?>

<body class="navbar-top">
	<?php
		// Inseri a barra fixa do topo
		include("includes/navbar-top.php");
	?>
	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">
			<?php
				// Inseri a barra fixa do topo
				include("includes/sidebar.php");
			?>	
			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Page header -->
				<div class="page-header">
					<div class="page-header-content">
						<div class="page-title">
							<h4><span class="text-semibold">Informações de Contato</span></h4>
						</div>
					</div>

				</div>
				<!-- /page header -->


				<!-- Content area -->
				<div class="content">
					<div class="row">
						<div class="col-sm-12">
							<div class="panel">
								<div class="panel-body">
									<form method="post" class="form-horizontal" id="form1">
										<input type="hidden" name="cmd" value="edit" />

										<div class="col-sm-12">
											<h5 class="panel-title">Endereço 1</h5>
											<br>
											<div class="row">
												<div class="col-sm-6">
													<div class="form-group">
														<label class="col-lg-2 text-right control-label">Título:</label>
														<div class="col-lg-10">
															<input type="text" name="titulo_endereco_1" class="form-control" value="<?=$vet_informacoes['titulo_endereco_1'];?>" required />
														</div>
													</div>
												</div>
												<div class="col-sm-6">
													<div class="form-group">
														<label class="col-lg-2 text-right control-label">Telefone:</label>
														<div class="col-lg-10">
															<input type="text" name="telefone_1" class="form-control" value="<?=$vet_informacoes['telefone_1'];?>" required />
														</div>
													</div>
												</div>
												<div class="col-sm-12">
													<div class="form-group">
														<label class="col-lg-1 text-right control-label">Endereço:</label>
														<div class="col-lg-11">
															<input type="text" name="endereco_1" class="form-control" value="<?=$vet_informacoes['endereco_1'];?>" required />
														</div>
													</div>
												</div>
											</div>
										</div>


										<div class="col-sm-12">
											<br><hr><br>
											<h5 class="panel-title">Endereço 2</h5>
											<br>
											<div class="row">
												<div class="col-sm-6">
													<div class="form-group">
														<label class="col-lg-2 text-right control-label">Título:</label>
														<div class="col-lg-10">
															<input type="text" name="titulo_endereco_2" class="form-control" value="<?=$vet_informacoes['titulo_endereco_2'];?>" required />
														</div>
													</div>
												</div>
												<div class="col-sm-6">
													<div class="form-group">
														<label class="col-lg-2 text-right control-label">Telefone:</label>
														<div class="col-lg-10">
															<input type="text" name="telefone_2" class="form-control" value="<?=$vet_informacoes['telefone_2'];?>" required />
														</div>
													</div>
												</div>
												<div class="col-sm-12">
													<div class="form-group">
														<label class="col-lg-1 text-right control-label">Endereço:</label>
														<div class="col-lg-11">
															<input type="text" name="endereco_2" class="form-control" value="<?=$vet_informacoes['endereco_2'];?>" required />
														</div>
													</div>
												</div>
											</div>
										</div>

										<div class="col-sm-12">
											<br><hr><br>
											<h5 class="panel-title">E-mails</h5>
											<br>
											<div class="row">

												<!--
													<div class="col-sm-6">
														<div class="form-group">
															<label class="col-lg-2 text-right control-label">Título 1:</label>
															<div class="col-lg-10">
																<input type="text" name="titulo_email_1" class="form-control" value="<?=$vet_informacoes['titulo_email_1'];?>" required />
															</div>
														</div>
													</div>
												-->
												<div class="col-sm-6">
													<div class="form-group">
														<label class="col-lg-2 text-right control-label">E-mail 1:</label>
														<div class="col-lg-10">
															<input type="text" name="email_1" class="form-control" value="<?=$vet_informacoes['email_1'];?>" />
														</div>
													</div>
												</div>

												<!--
													<div class="col-sm-6">
														<div class="form-group">
															<label class="col-lg-2 text-right control-label">Título 2:</label>
															<div class="col-lg-10">
																<input type="text" name="titulo_email_2" class="form-control" value="<?=$vet_informacoes['titulo_email_2'];?>" required />
															</div>
														</div>
													</div>
												-->
												<div class="col-sm-6">
													<div class="form-group">
														<label class="col-lg-2 text-right control-label">E-mail 2:</label>
														<div class="col-lg-10">
															<input type="text" name="email_2" class="form-control" value="<?=$vet_informacoes['email_2'];?>" />
														</div>
													</div>
												</div>

												<!--
													<div class="col-sm-6">
														<div class="form-group">
															<label class="col-lg-2 text-right control-label">Título 3:</label>
															<div class="col-lg-10">
																<input type="text" name="titulo_email_3" class="form-control" value="<?=$vet_informacoes['titulo_email_3'];?>" required />
															</div>
														</div>
													</div>
												-->
												<div class="col-sm-6">
													<div class="form-group">
														<label class="col-lg-2 text-right control-label">E-mail 3:</label>
														<div class="col-lg-10">
															<input type="text" name="email_3" class="form-control" value="<?=$vet_informacoes['email_3'];?>" />
														</div>
													</div>
												</div>
											</div>
										</div>

										<div class="col-sm-12">
											<br><hr><br>
											<h5 class="panel-title">Links Redes Sociais</h5>
											<br>
											<div class="row">

												<div class="col-sm-6">
													<div class="form-group">
														<label class="col-lg-2 text-right control-label">Facebook:</label>
														<div class="col-lg-10">
															<input type="text" name="link_facebook" class="form-control" value="<?=$vet_informacoes['link_facebook'];?>" required />
														</div>
													</div>
												</div>
												<div class="col-sm-6">
													<div class="form-group">
														<label class="col-lg-2 text-right control-label">Instagram:</label>
														<div class="col-lg-10">
															<input type="text" name="link_instagram" class="form-control" value="<?=$vet_informacoes['link_instagram'];?>" required />
														</div>
													</div>
												</div>
												<div class="col-sm-6">
													<div class="form-group">
														<label class="col-lg-2 text-right control-label">Twitter:</label>
														<div class="col-lg-10">
															<input type="text" name="link_twitter" class="form-control" value="<?=$vet_informacoes['link_twitter'];?>" required />
														</div>
													</div>
												</div>
												<div class="col-sm-6">
													<div class="form-group">
														<label class="col-lg-2 text-right control-label">Vimeo:</label>
														<div class="col-lg-10">
															<input type="text" name="link_vimeo" class="form-control" value="<?=$vet_informacoes['link_vimeo'];?>" required />
														</div>
													</div>
												</div>
												<div class="col-sm-6">
													<div class="form-group">
														<label class="col-lg-2 text-right control-label">Behance:</label>
														<div class="col-lg-10">
															<input type="text" name="link_behance" class="form-control" value="<?=$vet_informacoes['link_behance'];?>" required />
														</div>
													</div>
												</div>
												<div class="col-sm-6">
													<div class="form-group">
														<label class="col-lg-2 text-right control-label">Pinterest:</label>
														<div class="col-lg-10">
															<input type="text" name="link_pinterest" class="form-control" value="<?=$vet_informacoes['link_pinterest'];?>" required />
														</div>
													</div>
												</div>
												<div class="col-sm-6">
													<div class="form-group">
														<label class="col-lg-2 text-right control-label">Apple:</label>
														<div class="col-lg-10">
															<input type="text" name="link_apple" class="form-control" value="<?=$vet_informacoes['link_apple'];?>" required />
														</div>
													</div>
												</div>

											</div>
										</div>
									
										<div class="col-sm-12">
											<div class="text-right">
												<button type="submit" class="btn bg-teal-300">Alterar <i class="icon-arrow-right14 position-right"></i></button>
											</div>
										</div>

									</form>
								</div>
							</div>
						</div>
					</div>	

				</div>

<?php
	include("includes/footer.php");
?>