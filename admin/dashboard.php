<?php
	session_start();

	$include_js = '
	<script type="text/javascript" src="assets/js/plugins/media/fancybox.min.js"></script>

	<script type="text/javascript" src="assets/js/pages/user_pages_team.js"></script>

	<script type="text/javascript">
		$(function(){
			$("#link_sidebar_dashboard").addClass("active");
		});
	</script>';
	$include_ccs = "";
	include("includes/header.php");
	include("includes/verifica.php");
?>

<body class="navbar-top">
	<?php
		// Inseri a barra fixa do topo
		include("includes/navbar-top.php");
	?>
	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">
			<?php
				// Inseri a barra fixa do topo
				include("includes/sidebar.php");
			?>			
			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Page header -->
				<div class="page-header">
					<div class="page-header-content">
						<div class="page-title">
							<h3>Início</h3>
						</div>
					</div>

				</div>
				<!-- /page header -->


				<!-- Content area -->
				<div class="content">

					<div class="row">
						<div class="col-sm-12">
							<div class="panel">
								<div class="panel-body">
									<h2 class="text-light"><i class="icon-arrow-left52 position-left"></i>Para gerenciamento do site utilize as abas da lateral a esquerda.</h2>
									<br>
									<h4 class="text-light">Precisando de ajuda ou detectou alguma falha?</h4>
									<h6 class="text-light">Entre em contato:</h6>
									<p><b>Telefone:</b> (027) 99887-0591</p>
									<p><b>E-mail:</b> contato@gabrielprogramador.com.br</p>
								</div>
							</div>
						</div>
					</div>	

				</div>
<?php
	include("includes/footer.php");
?>