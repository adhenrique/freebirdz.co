<?php
	session_start();

	$include_js = '
	<script type="text/javascript" src="assets/js/plugins/media/fancybox.min.js"></script>

	<script type="text/javascript" src="assets/js/plugins/forms/selects/select2.min.js"></script>
	<script type="text/javascript" src="assets/js/plugins/forms/styling/uniform.min.js"></script>

	<script type="text/javascript" src="assets/js/pages/user_pages_team.js"></script>
	<script type="text/javascript" src="assets/js/pages/form_layouts.js"></script>

	<script type="text/javascript">
		$(function(){
			$("#link_sidebar_portfolio").addClass("active");
		});
	</script>';
	$include_ccs = "";
	include("includes/header.php");
	include("includes/verifica.php");

	if($_POST['cmd'] == "novo"){

		$titulo = addslashes($_POST['titulo']);
		$localidade = addslashes($_POST['localidade']);
		$ano = addslashes($_POST['ano']);
		$tipo_capa = $_POST['tipo_capa'];
		$tipo_portfolio = $_POST['tipo_portfolio'];
		$descricao = addslashes($_POST['descricao']);
		// $categorias_portfolio = $_POST['categorias_portfolio'];
		$nome_cliente = addslashes($_POST['nome_cliente']);
		$frase_cliente = addslashes($_POST['frase_cliente']);

		$titulo_en = addslashes($_POST['titulo_en']);
		$localidade_en = addslashes($_POST['localidade_en']);
		$descricao_en = addslashes($_POST['descricao_en']);
		$frase_cliente_en = addslashes($_POST['frase_cliente_en']);


		$capa = $_FILES['capa'];
		$background = $_FILES['background'];
		$logo = $_FILES['logo'];


		$query_insert_portfolio = "INSERT INTO portfolio (codigo, titulo, logo, img_capa, tipo_capa, localidade, ano, descricao, bg_topo, frase_cliente, nome_cliente, visualizacoes, data, titulo_en, localidade_en, descricao_en, frase_cliente_en) VALUES (0, '$titulo', '', '', '$tipo_capa', '$localidade', '$ano', '$descricao', '', '$frase_cliente', '$nome_cliente', 0, '$data_hora_eua', '$titulo_en', '$localidade_en', '$descricao_en', '$frase_cliente_en')";
		$result_insert_portfolio = mysql_query($query_insert_portfolio) or die(mysql_error());
		$id_insert_portfolio = mysql_insert_id();


		if($tipo_portfolio){
			foreach($tipo_portfolio as $value){
				mysql_query("INSERT INTO portfolio_tipos (codigo, codigo_portfolio, tipo) VALUES (0, '$id_insert_portfolio', '$value')");
			}
		}

		/*
		if($categorias_portfolio){
			foreach($categorias_portfolio as $value){
				mysql_query("INSERT INTO portfolio_categorias (codigo, codigo_portfolio, titulo_categoria) VALUES (0, '$id_insert_portfolio', '$value')");
			}
		}
		*/

		$diretorio = "../img/portfolio/";

		if($capa['tmp_name']){
			$extensao_capa = explode("/", $capa["type"]);
			$novo_nome_capa = "capa-" . $id_insert_portfolio . "-" . date('d-m-Y-H-i-s') . "-" . mt_rand(1111,9999) . "." . $extensao_capa[1];
			if(move_uploaded_file($capa["tmp_name"], $diretorio . $novo_nome_capa)){
				mysql_query("UPDATE portfolio SET img_capa = '$novo_nome_capa' WHERE codigo = '$id_insert_portfolio' LIMIT 1");
			}
		}

		if($background['tmp_name']){
			$extensao_bg = explode("/", $background["type"]);
			$novo_nome_background = "background-" . $id_insert_portfolio . "-" . date('d-m-Y-H-i-s') . "-" . mt_rand(1111,9999) . "." . $extensao_bg[1];
			if(move_uploaded_file($background["tmp_name"], $diretorio . $novo_nome_background)){
				mysql_query("UPDATE portfolio SET bg_topo = '$novo_nome_background' WHERE codigo = '$id_insert_portfolio' LIMIT 1");
			}
		}

		if($logo['tmp_name']){
			$extensao_logo = explode("/", $logo["type"]);
			$novo_nome_logo = "logo-" . $id_insert_portfolio . "-" . date('d-m-Y-H-i-s') . "-" . mt_rand(1111,9999) . "." . $extensao_logo[1];
			if(move_uploaded_file($logo["tmp_name"], $diretorio . $novo_nome_logo)){
				mysql_query("UPDATE portfolio SET logo = '$novo_nome_logo' WHERE codigo = '$id_insert_portfolio' LIMIT 1");
			}
		}


		redireciona("portfolio.php?msg_success=cria&codigo=" . $id_insert_portfolio);

	}

?>

<body class="navbar-top">
	<?php
		// Inseri a barra fixa do topo
		include("includes/navbar-top.php");
	?>
	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">
			<?php
				// Inseri a barra fixa do topo
				include("includes/sidebar.php");
			?>	
			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Page header -->
				<div class="page-header">
					<div class="page-header-content">
						<div class="page-title">
							<h4><span class="text-semibold">Portfólio</span> - Novo Portfólio</h4>
						</div>
					</div>

					<div class="breadcrumb-line">
						<ul class="breadcrumb">
							<li><a href="portfolio.php"><i class="icon-users4 position-left"></i> Portfólio</a></li>
							<li class="active">Novo Portfólio</li>
						</ul>
					</div>

				</div>
				<!-- /page header -->


				<!-- Content area -->
				<div class="content">

					<div class="row">
						<div class="col-sm-12">
							<div class="panel">
								<form method="post" class="form-horizontal" enctype="multipart/form-data">
									<input type="hidden" name="cmd" value="novo" />
									<div class="panel-body">
										<div class="row">
											<div class="col-sm-6">
												<div class="form-group">
													<label class="col-lg-4 control-label text-right"><img src="assets/images/baneira-pt.png" width="20px" style="position: relative; top: -1px;" /> Título do Portfólio:</label>
													<div class="col-lg-8">
														<input type="text" name="titulo" class="form-control" placeholder="" required />
													</div>
												</div>
											</div>
											<div class="col-sm-4">
												<div class="form-group">
													<label class="col-lg-4 control-label text-right"><img src="assets/images/baneira-pt.png" width="20px" style="position: relative; top: -1px;" /> Localidade:</label>
													<div class="col-lg-8">
														<input type="text" name="localidade" class="form-control" placeholder="" required />
													</div>
												</div>
											</div>
											<div class="col-sm-2">
												<div class="form-group">
													<label class="col-lg-4 control-label text-right">Ano:</label>
													<div class="col-lg-8">
														<input type="text" name="ano" class="form-control" placeholder="" required />
													</div>
												</div>
											</div>
											<div class="col-sm-6">
												<div class="form-group">
													<label class="col-lg-4 control-label text-right"><img src="assets/images/baneira-en.png" width="20px" style="position: relative; top: -1px;" /> Título do Portfólio:</label>
													<div class="col-lg-8">
														<input type="text" name="titulo_en" class="form-control" placeholder="" required />
													</div>
												</div>
											</div>
											<div class="col-sm-4">
												<div class="form-group">
													<label class="col-lg-4 control-label text-right"><img src="assets/images/baneira-en.png" width="20px" style="position: relative; top: -1px;" /> Localidade:</label>
													<div class="col-lg-8">
														<input type="text" name="localidade_en" class="form-control" placeholder="" required />
													</div>
												</div>
											</div>
										</div>
										<br>
										<div class="row">
											<div class="col-sm-6">
												<div class="form-group">
													<label class="col-lg-4 control-label text-right">Imagem da Capa:</label>
													<div class="col-lg-8">
														<div class="media no-margin-top">
															<div class="media-left">
																<img src="assets/images/img_default.jpg" style="width: 58px; height: 58px;" class="img-rounded" alt="">
															</div>
															<div class="media-body">
																<input type="file" name="capa" class="file-styled" required />
																<span class="help-block">Formatos suportados: png e jpg.<br>Tamanho máximo recomendado 1Mb</span>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="col-sm-5">
												<div class="form-group" id="tipo_capa">
													<label class="col-lg-3 control-label text-right">Tipo de Capa:</label>
													<div class="col-lg-9">
														<label class="radio-inline">
															<input type="radio" class="styled" name="tipo_capa" value="1" checked="checked">
															Background
														</label>
														&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
														<label class="radio-inline">
															<input type="radio" class="styled" name="tipo_capa" value="2">
															Logo em PNG
														</label>
													</div>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-6">
												<div class="form-group">
													<label class="col-lg-4 control-label text-right">Logo do Portfólio:</label>
													<div class="col-lg-8">
														<div class="media no-margin-top">
															<div class="media-left">
																<img src="assets/images/logo_default.jpg" style="width: 58px; height: 58px;" class="img-rounded" alt="">
															</div>
															<div class="media-body">
																<input type="file" name="logo" class="file-styled" required />
																<span class="help-block">Formatos suportados: png e jpg.<br>Tamanho máximo recomendado 1Mb</span>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="col-sm-6">
												<div class="form-group" id="tipo_portfolio">
													<label class="col-lg-3 control-label text-right">Tipo de Portfólio:</label>
													<div class="col-lg-8">
														<label class="checkbox-inline">
															<input type="checkbox" class="styled" name="tipo_portfolio[]" value="1">
															Filme
														</label>
														&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
														<label class="checkbox-inline">
															<input type="checkbox" class="styled" name="tipo_portfolio[]" value="2">
															Design
														</label>
														<label class="checkbox-inline">
															<input type="checkbox" class="styled" name="tipo_portfolio[]" value="3">
															Música
														</label>
														&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
													</div>
												</div>
											</div>

											<div class="col-sm-12">
												<div class="form-group">
													<label class="col-lg-2 control-label text-right"><img src="assets/images/baneira-pt.png" width="20px" style="position: relative; top: -1px;" /> Descrição do Portfólio:</label>
													<div class="col-lg-10">
														<textarea name="descricao" class="form-control" style="resize: none;" required></textarea>
														<span class="help-block">Será utilizado para SEO para <i>meta="description"</i>. procure utilizar até 80 caracteres.</span>
													</div>
												</div>
												<div class="form-group">
													<label class="col-lg-2 control-label text-right"><img src="assets/images/baneira-en.png" width="20px" style="position: relative; top: -1px;" /> Descrição do Portfólio:</label>
													<div class="col-lg-10">
														<textarea name="descricao_en" class="form-control" style="resize: none;" required></textarea>
														<span class="help-block">Será utilizado para SEO para <i>meta="description"</i>. procure utilizar até 80 caracteres.</span>
													</div>
												</div>
											</div>
										</div>

										<br><br><br>

										<div class="row">
											<!--
											<div class="col-sm-6 categorias_portfolio">
												<div class="row">
													<label class="col-lg-4 control-label text-right"><img src="assets/images/baneira-pt.png" width="20px" style="position: relative; top: -1px;" /> Categorias do Portfólio:</label>
													<div class="col-lg-8">
														<div class="inputs">
															<div class="input-group form-group">
																<input type="text" class="form-control" name="categorias_portfolio[]" />
																<span class="input-group-btn">
																	<button disabled class="btn btn-danger" onclick="categoriaConteudos(0, $(this));" type="button"><b>-</b></button>
																</span>
															</div>
														</div>
														<center>
															<button class="btn btn-xs bg-teal" onclick="categoriaConteudos(1, $(this));" type="button"><b>+ ADICIONAR CATEGORIA</b></button>
														</center>
													</div>
												</div>
											</div>

											<script>
												function categoriaConteudos(acao, element){

													var inputGroup = '<div class="input-group form-group"><input type="text" class="form-control" name="categorias_portfolio[]" /><span class="input-group-btn"><button class="btn btn-danger" onclick="categoriaConteudos(0, $(this));" type="button"><b>-</b></button></span></div>';

													if(acao == 1){
														$(".categorias_portfolio .inputs").append(inputGroup);
													}
													else if(acao == 0){
														$(element).parent().parent().remove();
													}

													if($(".categorias_portfolio .inputs .input-group").size() == 1){
														$(".categorias_portfolio .inputs .input-group button").prop("disabled", true);
													}
													else{
														$(".categorias_portfolio .inputs .input-group button").removeAttr("disabled");
													}
												}
											</script>
										-->

											<div class="col-sm-6">
												<div class="form-group">
													<label class="col-lg-4 control-label text-right">Nome do Cliente:</label>
													<div class="col-lg-8">
														<input type="text" name="nome_cliente" class="form-control" placeholder="" required />
													</div>
												</div>
											</div>
											<div class="col-sm-6">
												<div class="form-group">
													<label class="col-lg-4 control-label text-right">Background do Topo:</label>
													<div class="col-lg-8">
														<div class="media no-margin-top">
															<div class="media-left">
																<img src="assets/images/img_default.jpg" style="width: 58px; height: 58px;" class="img-rounded" alt="">
															</div>
															<div class="media-body">
																<input type="file" name="background" class="file-styled" required />
																<span class="help-block">Formatos suportados: png e jpg.<br>Tamanho máximo recomendado 1.5Mb</span>
															</div>
														</div>
													</div>
												</div>
											</div>

											<div class="col-sm-12">
												<div class="form-group">
													<label class="col-lg-2 control-label text-right"><img src="assets/images/baneira-pt.png" width="20px" style="position: relative; top: -1px;" /> Frase do Cliente:</label>
													<div class="col-lg-10">
														<textarea name="frase_cliente" class="form-control" style="resize: none;" required></textarea>
													</div>
												</div>
												<div class="form-group">
													<label class="col-lg-2 control-label text-right"><img src="assets/images/baneira-en.png" width="20px" style="position: relative; top: -1px;" /> Frase do Cliente:</label>
													<div class="col-lg-10">
														<textarea name="frase_cliente_en" class="form-control" style="resize: none;" required></textarea>
													</div>
												</div>
											</div>
										</div>

										<br>

										<div class="row">

											<div class="col-sm-12">
												<div class="text-right">
													<button type="submit" class="btn bg-teal-300">Criar <i class="icon-arrow-right14 position-right"></i></button>
												</div>
											</div>

										</div>

									</div>
								</form>
							</div>
						</div>
					</div>	

				</div>


<?php
	include("includes/footer.php");
?>
