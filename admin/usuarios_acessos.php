<?php
	session_start();

	$include_js = '
	<script type="text/javascript" src="assets/js/plugins/media/fancybox.min.js"></script>

	<script type="text/javascript" src="assets/js/pages/user_pages_team.js"></script>

	<script type="text/javascript">
		$(function(){
			$("#link_sidebar_usuarios").addClass("active");
		});
	</script>';
	$include_ccs = "";
	include("includes/header.php");
	include("includes/verifica.php");

	if($_SESSION['admin'] == 0){
		redireciona('dashboard.php');
		die;
	}

	if($_GET['acesso']){
		$usuario_acesso = anti_injection($_GET['acesso']);
	}
	else{
		redireciona("usuarios.php");
	}

	$query_user = "SELECT * FROM usuarios WHERE codigo = '$usuario_acesso'";
	$result_user = mysql_query($query_user) or die (mysql_error());
	$vet_user = mysql_fetch_array($result_user);

	// Paginação
	// Pegar a página atual por GET 
	$pag = $_GET['pag'];

	// Verifica se a variável tá declarada, senão deixa na primeira página como padrão
	if(!isset($pag)){
		$pag = 1;
	}

	// Define a quantidade máxima de itens por página
	$qnt = $_GET['qnt'];
	if(!isset($qnt)){
		$qnt = 15;
	}

	// Calculo de inicio od sitens a serem exibidos
	$inicio = ($pag * $qnt) - $qnt;

?>

<body class="navbar-top">
	<?php
		// Inseri a barra fixa do topo
		include("includes/navbar-top.php");
	?>
	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">
			<?php
				// Inseri a barra fixa do topo
				include("includes/sidebar.php");
			?>	
			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Page header -->
				<div class="page-header">
					<div class="page-header-content">
						<div class="page-title">
							<h4><span class="text-semibold">Usuários</span> - Ver acessos</h4>
						</div>
					</div>
					<div class="breadcrumb-line">
						<ul class="breadcrumb">
							<li><a href="usuarios.php"><i class="icon-users4 position-left"></i> Usuários</a></li>
							<li class="active">Ver acessos</li>
						</ul>
					</div>

				</div>
				<!-- /page header -->


				<!-- Content area -->
				<div class="content">

					<div class="row">
						<div class="col-sm-12">
							<div class="panel">
								<div class="panel-heading">
									<div class="pull-right">
										<?php
											$query_acessos_total = "SELECT * FROM acessos WHERE idusuario = '$usuario_acesso' ORDER BY codigo DESC";
											$result_acessos_total = mysql_query($query_acessos_total) or die(mysql_error());
											$row_acessos_total = mysql_num_rows($result_acessos_total);
											
											if($row_acessos_total > 0){
												$vet_acessos_total = mysql_fetch_array($result_acessos_total);
												$qnt_acessos_total = $vet_acessos_total['acesso_num'];
												if($qnt_acessos_total > 1){
													echo "<span class=\"label border-right-teal label-striped label-striped-right\">" . $qnt_acessos_total . " ACESSOS NO TOTAL</span>";
												}
												else{
													echo "<span class=\"label border-right-teal label-striped label-striped-right\">1 ACESSO NO TOTAL</span>";
												}
											}
											else{
												echo "<span class=\"label border-right-teal label-striped label-striped-right\">NENHUM ACESSO</span>";
											}
										?>
									</div>
									<h5 class="panel-title">Dados de Acesso</h5>
								</div>

								<div class="panel-body">
									Confira abaixo os acessos do usuário <span class="text-bold"><?php echo $vet_user['login']; ?></span> (<em><?php echo $vet_user['nome'] . ' ' . $vet_user['sobrenome']; ?></em>) com seu respectivo número do acesso, data, hora e local de acesso.
								</div>

								<div class="table-responsive">
									<table class="table table-striped">
										<thead>
											<tr>
												<th>Nº do Acesso</th>
												<th>Data</th>
												<th>Hora</th>
											</tr>
										</thead>
										<tbody>
											<?php
												$query_acessos = "SELECT * FROM acessos WHERE idusuario = '$usuario_acesso' ORDER BY codigo DESC LIMIT $inicio, $qnt";
												$result_acessos = mysql_query($query_acessos) or die(mysql_error());
												$row_acessos = mysql_num_rows($result_acessos);

												if($row_acessos > 0){
													while($vet_acessos = mysql_fetch_array($result_acessos)){
														$data = substr($vet_acessos['data'], 0, 10); 
														$hora = substr($vet_acessos['data'], 11, 8); 

														$data = explode("-", $data);
														$data = $data[2] . "/" . $data[1] . "/" . $data[0];
											?>
											<tr>
												<td><?php echo $vet_acessos['acesso_num']; ?></td>
												<td><?php echo $data; ?></td>
												<td><?php echo $hora; ?></td>
											</tr>
											<?php
													}
												}
											?>
										</tbody>
									</table>
								</div>
								<div class="panel-body text-center">
									<div class="pull-right">
										<div class="pull-left"><p class="text-right" style="margin-right:5px; line-height:1.3em;">Quantidade de dados<br>por página:</p></div>
										<div class="btn-group">
					                    	<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"><?php echo $qnt; ?> <span class="caret"></span></button>
					                    	<ul class="dropdown-menu dropdown-menu-right" style="min-width:53px;">
												<li><a href="usuarios_acessos.php?acesso=<?php echo $usuario_acesso; ?>&qnt=15">15</a></li>
												<li><a href="usuarios_acessos.php?acesso=<?php echo $usuario_acesso; ?>&qnt=30">30</a></li>
												<li><a href="usuarios_acessos.php?acesso=<?php echo $usuario_acesso; ?>&qnt=50">50</a></li>
											</ul>
										</div>
									</div>
									<ul class="pagination">
										<?php
											$query_acessos = "SELECT * FROM acessos WHERE idusuario = '$usuario_acesso'";
											$result_acessos = mysql_query($query_acessos) or die(mysql_error());
											$row_acessos = mysql_num_rows($result_acessos);

											$pags = ceil($row_acessos/$qnt);
											$max_links_pag = 4;

											if($pag > 1){
												echo "<li><a href=\"usuarios_acessos.php?acesso=" . $usuario_acesso . "&pag=" . ($pag - 1) . "&qnt=" . $qnt . "\">&lsaquo;</a></li>";
											}

											for($i = $pag - $max_links_pag; $i <= $pag - 1; $i++){
												if($i <= 0){
													// não faz nada
												}
												else{
													echo "<li><a href=\"usuarios_acessos.php?acesso=" . $usuario_acesso . "&pag=" . $i . "&qnt=" . $qnt . "\">" . $i . "</a></li>";
												}
											}

											echo "<li class=\"active\"><a href=\"#\">" . $pag . "</a></li>";

											for($i = $pag + 1; $i <= $pag + $max_links_pag; $i++){
												if($i > $pags){
													// não faz nada
												}
												else{
													echo "<li><a href=\"usuarios_acessos.php?acesso=" . $usuario_acesso . "&pag=" . $i . "&qnt=" . $qnt . "\">" . $i . "</a></li>";
												}
											}

											if($pag != $pags){
												echo "<li><a href=\"usuarios_acessos.php?acesso=" . $usuario_acesso . "&pag=" . ($pag + 1) . "&qnt=" . $qnt . "\">&rsaquo;</a></li>";
											}
										?>
									</ul>
								</div>
							</div>
						</div>
					</div>	

					</div>
					<!-- /dropdown menu -->
<?php
	include("includes/footer.php");
?>