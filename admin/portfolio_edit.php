<?php
	session_start();

	$include_js = '
	<script type="text/javascript" src="assets/js/plugins/media/fancybox.min.js"></script>

	<script type="text/javascript" src="assets/js/plugins/forms/selects/select2.min.js"></script>
	<script type="text/javascript" src="assets/js/plugins/forms/styling/uniform.min.js"></script>

	<script type="text/javascript" src="assets/js/pages/user_pages_team.js"></script>
	<script type="text/javascript" src="assets/js/pages/form_layouts.js"></script>

	<script type="text/javascript">
		$(function(){
			$("#link_sidebar_portfolio").addClass("active");
		});
	</script>';
	include("includes/header.php");
	include("includes/verifica.php");


	if($_POST['cmd'] == "edit"){

		$portfolio_edit = $_POST['portfolio_edit'];

		$titulo = addslashes($_POST['titulo']);
		$localidade = addslashes($_POST['localidade']);
		$ano = addslashes($_POST['ano']);
		$tipo_capa = $_POST['tipo_capa'];
		$tipo_portfolio = $_POST['tipo_portfolio'];
		$descricao = addslashes($_POST['descricao']);
		$categorias_portfolio = $_POST['categorias_portfolio'];
		$nome_cliente = addslashes($_POST['nome_cliente']);
		$frase_cliente = addslashes($_POST['frase_cliente']);

		$titulo_en = addslashes($_POST['titulo_en']);
		$localidade_en = addslashes($_POST['localidade_en']);
		$descricao_en = addslashes($_POST['descricao_en']);
		$frase_cliente_en = addslashes($_POST['frase_cliente_en']);

		$capa = $_FILES['capa'];
		$background = $_FILES['background'];
		$logo = $_FILES['logo'];

		$query_consulta_portfolio = "SELECT * FROM portfolio WHERE codigo = '$portfolio_edit' LIMIT 1";
		$result_consulta_portfolio = mysql_query($query_consulta_portfolio) or die(mysql_error());
		$vet_consulta_portfolio = mysql_fetch_array($result_consulta_portfolio);

		mysql_query("UPDATE portfolio SET titulo = '$titulo', tipo_capa = '$tipo_capa', localidade = '$localidade', ano = '$ano', descricao = '$descricao', frase_cliente = '$frase_cliente', nome_cliente = '$nome_cliente', titulo_en = '$titulo_en', localidade_en = '$localidade_en', descricao_en = '$descricao_en', frase_cliente_en = '$frase_cliente_en'  WHERE codigo = '$portfolio_edit' LIMIT 1");


		mysql_query("DELETE FROM portfolio_tipos WHERE codigo_portfolio = '$portfolio_edit'");
		if($tipo_portfolio){
			foreach($tipo_portfolio as $value){
				mysql_query("INSERT INTO portfolio_tipos (codigo, codigo_portfolio, tipo) VALUES (0, '$portfolio_edit', '$value')");
			}
		}

		$diretorio = "../img/portfolio/";

		if($capa['tmp_name']){
			$extensao_capa = explode("/", $capa["type"]);
			$novo_nome_capa = "capa-" . $portfolio_edit . "-" . date('d-m-Y-H-i-s') . "-" . mt_rand(1111,9999) . "." . $extensao_capa[1];
			if(move_uploaded_file($capa["tmp_name"], $diretorio . $novo_nome_capa)){
				mysql_query("UPDATE portfolio SET img_capa = '$novo_nome_capa' WHERE codigo = '$portfolio_edit' LIMIT 1");

				if($vet_consulta_portfolio['img_capa']){
					unlink($diretorio . $vet_consulta_portfolio['img_capa']);
				}
			}
		}

		if($background['tmp_name']){
			$extensao_bg = explode("/", $background["type"]);
			$novo_nome_background = "background-" . $portfolio_edit . "-" . date('d-m-Y-H-i-s') . "-" . mt_rand(1111,9999) . "." . $extensao_bg[1];
			if(move_uploaded_file($background["tmp_name"], $diretorio . $novo_nome_background)){
				mysql_query("UPDATE portfolio SET bg_topo = '$novo_nome_background' WHERE codigo = '$portfolio_edit' LIMIT 1");

				if($vet_consulta_portfolio['bg_topo']){
					unlink($diretorio . $vet_consulta_portfolio['bg_topo']);
				}
			}
		}

		if($logo['tmp_name']){
			$extensao_logo = explode("/", $logo["type"]);
			$novo_nome_logo = "logo-" . $portfolio_edit . "-" . date('d-m-Y-H-i-s') . "-" . mt_rand(1111,9999) . "." . $extensao_logo[1];
			if(move_uploaded_file($logo["tmp_name"], $diretorio . $novo_nome_logo)){
				mysql_query("UPDATE portfolio SET logo = '$novo_nome_logo' WHERE codigo = '$portfolio_edit' LIMIT 1");

				if($vet_consulta_portfolio['logo']){
					unlink($diretorio . $vet_consulta_portfolio['logo']);
				}
			}
		}


		echo "<script>$(function(){ $(\".alert_success#msg1\").removeClass(\"hide\"); });</script>";
		
	}


	if($_GET['edit']){
		$portfolio_edit = anti_injection($_GET['edit']);

		$query_portfolio = "SELECT * FROM portfolio WHERE codigo = '$portfolio_edit' LIMIT 1";
		$result_portfolio = mysql_query($query_portfolio) or die(mysql_error());
		$row_portfolio = mysql_num_rows($result_portfolio);

		if($row_portfolio > 0){
			$vet_portfolio = mysql_fetch_array($result_portfolio);
		}
		else{
			redireciona("portfolio.php");
		}
	}
	else{
		redireciona("portfolio.php");
	}

	function tipoPortfolio($codigo_portfolio, $tipo){
		$query_tipo = "SELECT * FROM portfolio_tipos WHERE codigo_portfolio = '$codigo_portfolio' AND tipo = '$tipo'";
		$result_tipo = mysql_query($query_tipo) or die(mysql_error());
		if(mysql_num_rows($result_tipo) != 0){
			return "checked=\"checked\"";
		}
	}

?>

<body class="navbar-top">
	<?php
		// Inseri a barra fixa do topo
		include("includes/navbar-top.php");
	?>
	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">
			<?php
				// Inseri a barra fixa do topo
				include("includes/sidebar.php");
			?>	
			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Page header -->
				<div class="page-header">
					<div class="page-header-content">
						<div class="page-title">
							<h4><span class="text-semibold">Portfólio</span> - Alterar Portfólio</h4>
						</div>
					</div>

					<div class="breadcrumb-line">
						<ul class="breadcrumb">
							<li><a href="portfolio.php"><i class="icon-users4 position-left"></i> Portfólio</a></li>
							<li class="active">Alterar Portfólio</li>
						</ul>
					</div>

				</div>
				<!-- /page header -->


				<!-- Content area -->
				<div class="content">

					<div class="row">
						<div class="col-sm-12">
							<div class="alert  alert-bordered alert-success hide alert_success" id="msg1">
								<button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Fechar</span></button>
								Alterações no portfólio realizadas com sucesso! 
						  	</div>
							<div class="panel">
								<form method="post" class="form-horizontal" enctype="multipart/form-data">
									<input type="hidden" name="cmd" value="edit" />
									<input type="hidden" name="portfolio_edit" value="<?=$portfolio_edit;?>">
									<div class="panel-body">
										<div class="row">
											<div class="col-sm-6">
												<div class="form-group">
													<label class="col-lg-4 control-label text-right"><img src="assets/images/baneira-pt.png" width="20px" style="position: relative; top: -1px;" /> Título do Portfólio:</label>
													<div class="col-lg-8">
														<input type="text" name="titulo" class="form-control" placeholder="" value="<?=$vet_portfolio['titulo'];?>" required />
													</div>
												</div>
											</div>
											<div class="col-sm-4">
												<div class="form-group">
													<label class="col-lg-4 control-label text-right"><img src="assets/images/baneira-pt.png" width="20px" style="position: relative; top: -1px;" /> Localidade:</label>
													<div class="col-lg-8">
														<input type="text" name="localidade" class="form-control" placeholder="" value="<?=$vet_portfolio['localidade'];?>" required />
													</div>
												</div>
											</div>
											<div class="col-sm-2">
												<div class="form-group">
													<label class="col-lg-4 control-label text-right">Ano:</label>
													<div class="col-lg-8">
														<input type="text" name="ano" class="form-control" placeholder="" value="<?=$vet_portfolio['ano'];?>" required />
													</div>
												</div>
											</div>
											<div class="col-sm-6">
												<div class="form-group">
													<label class="col-lg-4 control-label text-right"><img src="assets/images/baneira-en.png" width="20px" style="position: relative; top: -1px;" /> Título do Portfólio:</label>
													<div class="col-lg-8">
														<input type="text" name="titulo_en" class="form-control" placeholder="" value="<?=$vet_portfolio['titulo_en'];?>" required />
													</div>
												</div>
											</div>
											<div class="col-sm-4">
												<div class="form-group">
													<label class="col-lg-4 control-label text-right"><img src="assets/images/baneira-en.png" width="20px" style="position: relative; top: -1px;" /> Localidade:</label>
													<div class="col-lg-8">
														<input type="text" name="localidade_en" class="form-control" placeholder="" value="<?=$vet_portfolio['localidade_en'];?>" required />
													</div>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-6">
												<div class="form-group">
													<label class="col-lg-4 control-label text-right">Imagem da Capa:</label>
													<div class="col-lg-8">
														<div class="media no-margin-top">
															<div class="media-left">
																<img src="../img/portfolio/<?=$vet_portfolio['img_capa'];?>" style="width: 58px; height: 58px;" class="img-rounded" alt="">
															</div>
															<div class="media-body">
																<input type="file" name="capa" class="file-styled" />
																<span class="help-block">Formatos suportados: png e jpg.<br>Tamanho máximo recomendado 1Mb</span>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="col-sm-5">
												<div class="form-group" id="tipo_capa">
													<label class="col-lg-3 control-label text-right">Tipo de Capa:</label>
													<div class="col-lg-9">
														<label class="radio-inline">
															<input type="radio" class="styled" name="tipo_capa" value="1" <? echo($vet_portfolio['tipo_capa'] == 1 ? "checked=\"checked\"" : "");?>>
															Background
														</label>
														&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
														<label class="radio-inline">
															<input type="radio" class="styled" name="tipo_capa" value="2" <? echo($vet_portfolio['tipo_capa'] == 2 ? "checked=\"checked\"" : "");?>>
															Logo em PNG
														</label>
													</div>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-6">
												<div class="form-group">
													<label class="col-lg-4 control-label text-right">Logo do Portfólio:</label>
													<div class="col-lg-8">
														<div class="media no-margin-top">
															<div class="media-left">
																<img src="../img/portfolio/<?=$vet_portfolio['logo'];?>" style="width: 58px; height: 58px;" class="img-rounded" alt="">
															</div>
															<div class="media-body">
																<input type="file" name="logo" class="file-styled" />
																<span class="help-block">Formatos suportados: png e jpg.<br>Tamanho máximo recomendado 1Mb</span>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="col-sm-6">
												<div class="form-group" id="tipo_portfolio">
													<label class="col-lg-3 control-label text-right">Tipo de Portfólio:</label>
													<div class="col-lg-8">
														<label class="checkbox-inline">
															<input type="checkbox" class="styled" name="tipo_portfolio[]" <?=tipoPortfolio($portfolio_edit, 1);?> value="1">
															Filme
														</label>
														&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
														<label class="checkbox-inline">
															<input type="checkbox" class="styled" name="tipo_portfolio[]" <?=tipoPortfolio($portfolio_edit, 2);?> value="2">
															Design
														</label>
														<label class="checkbox-inline">
															<input type="checkbox" class="styled" name="tipo_portfolio[]" <?=tipoPortfolio($portfolio_edit, 3);?> value="3">
															Música
														</label>
														&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
													</div>
												</div>
											</div>

											<div class="col-sm-12">
												<div class="form-group">
													<label class="col-lg-2 control-label text-right"><img src="assets/images/baneira-pt.png" width="20px" style="position: relative; top: -1px;" /> Descrição do Portfólio:</label>
													<div class="col-lg-10">
														<textarea name="descricao" class="form-control" style="resize: none;" required><?=$vet_portfolio['descricao'];?></textarea>
														<span class="help-block">Será utilizado para SEO para <i>meta="description"</i>. procure utilizar até 80 caracteres.</span>
													</div>
												</div>
												<div class="form-group">
													<label class="col-lg-2 control-label text-right"><img src="assets/images/baneira-en.png" width="20px" style="position: relative; top: -1px;" /> Descrição do Portfólio:</label>
													<div class="col-lg-10">
														<textarea name="descricao_en" class="form-control" style="resize: none;" required><?=$vet_portfolio['descricao_en'];?></textarea>
														<span class="help-block">Será utilizado para SEO para <i>meta="description"</i>. procure utilizar até 80 caracteres.</span>
													</div>
												</div>
											</div>
										</div>

										<br><br>

										<div class="row">
											<!--
											<div class="col-sm-6 categorias_portfolio">
												<div class="row">
													<label class="col-lg-4 control-label text-right">Categorias dos Conteúdos:</label>
													<div class="col-lg-8">
														<div class="inputs">
															<?
																$query_categorias = "SELECT * FROM portfolio_categorias WHERE codigo_portfolio = '$portfolio_edit'";
																$result_categorias = mysql_query($query_categorias) or die(mysql_error());
																
																$disabled = "";

																if(mysql_num_rows($result_categorias) <= 1){
																	$disabled = "disabled";
																}

																while($vet_categoria = mysql_fetch_array($result_categorias)){
															?>
																<div class="input-group form-group">
																	<input type="text" class="form-control" name="categorias_portfolio[]" value="<?=$vet_categoria['titulo_categoria'];?>" />
																	<span class="input-group-btn">
																		<button <?=$disabled;?> class="btn btn-danger" onclick="categoriaConteudos(0, $(this));" type="button"><b>-</b></button>
																	</span>
																</div>
															<?
																}
																if(mysql_num_rows($result_categorias) == 0){
															?>
																<div class="input-group form-group">
																	<input type="text" class="form-control" name="categorias_portfolio[]" />
																	<span class="input-group-btn">
																		<button disabled class="btn btn-danger" onclick="categoriaConteudos(0, $(this));" type="button"><b>-</b></button>
																	</span>
																</div>
															<?
																}
															?>
														</div>
														<center>
															<button class="btn btn-xs bg-teal" onclick="categoriaConteudos(1, $(this));" type="button"><b>+ ADICIONAR CATEGORIA</b></button>
														</center>
													</div>
												</div>
											</div>

											<script>
												function categoriaConteudos(acao, element){

													var inputGroup = '<div class="input-group form-group"><input type="text" class="form-control" name="categorias_portfolio[]" /><span class="input-group-btn"><button class="btn btn-danger" onclick="categoriaConteudos(0, $(this));" type="button"><b>-</b></button></span></div>';

													if(acao == 1){
														$(".categorias_portfolio .inputs").append(inputGroup);
													}
													else if(acao == 0){
														$(element).parent().parent().remove();
													}

													if($(".categorias_portfolio .inputs .input-group").size() == 1){
														$(".categorias_portfolio .inputs .input-group button").prop("disabled", true);
													}
													else{
														$(".categorias_portfolio .inputs .input-group button").removeAttr("disabled");
													}
												}
											</script>
											-->

											<div class="col-sm-6">
												<div class="form-group">
													<label class="col-lg-4 control-label text-right">Nome do Cliente:</label>
													<div class="col-lg-8">
														<input type="text" name="nome_cliente" class="form-control" value="<?=$vet_portfolio['nome_cliente'];?>" placeholder="" required />
													</div>
												</div>
											</div>
											<div class="col-sm-6">
												<div class="form-group">
													<label class="col-lg-4 control-label text-right">Background do Topo:</label>
													<div class="col-lg-8">
														<div class="media no-margin-top">
															<div class="media-left">
																<img src="../img/portfolio/<?=$vet_portfolio['bg_topo'];?>" style="width: 80px; height: 32px;" class="img-rounded" alt="">
															</div>
															<div class="media-body">
																<input type="file" name="background" class="file-styled" />
																<span class="help-block">Formatos suportados: png e jpg.<br>Tamanho máximo recomendado 1.5Mb</span>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="col-sm-12">
												<div class="form-group">
													<label class="col-lg-2 control-label text-right"><img src="assets/images/baneira-pt.png" width="20px" style="position: relative; top: -1px;" /> Frase do Cliente:</label>
													<div class="col-lg-10">
														<textarea name="frase_cliente" class="form-control" style="resize: none;" required><?=$vet_portfolio['frase_cliente'];?></textarea>
													</div>
												</div>
												<div class="form-group">
													<label class="col-lg-2 control-label text-right"><img src="assets/images/baneira-en.png" width="20px" style="position: relative; top: -1px;" /> Frase do Cliente:</label>
													<div class="col-lg-10">
														<textarea name="frase_cliente_en" class="form-control" style="resize: none;" required><?=$vet_portfolio['frase_cliente_en'];?></textarea>
													</div>
												</div>
											</div>
										</div>

										<br>

										<div class="row">


											<div class="col-sm-12">
												<div class="text-right">
													<button type="submit" class="btn bg-teal-300">Alterar <i class="icon-arrow-right14 position-right"></i></button>
												</div>
											</div>

										</div>

									</div>
								</form>
							</div>
						</div>
					</div>	

				</div>

<?php
	include("includes/footer.php");
?>
