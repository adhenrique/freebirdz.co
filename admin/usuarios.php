<?php
	session_start();

	$include_js = '
	<script type="text/javascript" src="assets/js/plugins/media/fancybox.min.js"></script>

	<script type="text/javascript" src="assets/js/pages/user_pages_team.js"></script>

	<script type="text/javascript">
		$(function(){
			$("#link_sidebar_usuarios").addClass("active");
		});
	</script>';
	$include_ccs = "";
	include("includes/header.php");
	include("includes/verifica.php");

	if($_SESSION['admin'] == 0){
		redireciona('dashboard.php');
		die;
	}

	if(isset($_GET['ativa'])){
		
		$codigo_usuario = anti_injection($_GET['ativa']);
		$query = "UPDATE usuarios SET status = '1' WHERE codigo = '$codigo_usuario'";
		$result = mysql_query($query) or die(mysql_error());

		redireciona('usuarios.php?msg_success=ativa&codigo=' . $codigo_usuario);
	}
	if(isset($_GET['inativa'])){
		
		$codigo_usuario = anti_injection($_GET['inativa']);
		if($codigo_usuario == $_SESSION['adm_codigo']){

			redireciona('usuarios.php?msg_error=1');
		}
		else{
			$query = "UPDATE usuarios SET status = '0' WHERE codigo = '$codigo_usuario'";
			$result = mysql_query($query) or die(mysql_error());
			
			redireciona('usuarios.php?msg_success=inativa&codigo=' . $codigo_usuario);
		}
	}
	if(isset($_GET['delet'])){
		$id_delet = $_GET['delet'];
		
		if($id_delet == $_SESSION['adm_codigo']){
			redireciona('usuarios.php?msg_error=2');
		}
		else{

			$query = "SELECT * FROM usuarios WHERE codigo = '$id_delet'";
			$result = mysql_query($query) or die (mysql_error());
			$vet = mysql_fetch_array($result);

			if($vet['img_perfil']){
				unlink("uploads/perfil/" . $vet['img_perfil']);
			}

			$query_delet = "DELETE FROM usuarios WHERE codigo = '$id_delet'";
			$result_delet = mysql_query($query_delet) or die(mysql_error());
			
			redireciona('usuarios.php?msg_success=deleta&user='. $vet['login']);
		}
	}

	if(isset($_GET['msg_success'])){
		$codigo_usuario = anti_injection($_GET['codigo']);
		$query = "SELECT * FROM usuarios WHERE codigo = '$codigo_usuario'";
		$result = mysql_query($query) or die(mysql_error());
		$vet = mysql_fetch_array($result);
		$usuario = $vet['login'];
		$nome = $vet['nome'] . ' ' . $vet['sobrenome'];

		echo "<script>$(function(){ $(\".alert_success\").removeClass(\"hide\"); });</script>";
		
		if($_GET['msg_success'] == 'ativa'){
			echo "<script>$(function(){ $(\".alert_success font\").html(\"Usuário <span class='text-semibold'>" . $usuario . "</span> (" . $nome . ") ativado com sucesso!<br>A partir de agora esse usuário tem acesso ao gerenciador usando seu usuário e senha.\"); });</script>";
		}
		else if($_GET['msg_success'] == 'inativa'){
			echo "<script>$(function(){ $(\".alert_success font\").html(\"Usuário <span class='text-semibold'>" . $usuario . "</span> (" . $nome . ") inativo com sucesso!<br>A partir de agora esse usuário tem acesso ao gerenciador bloqueado.\"); });</script>";
		}
		else if($_GET['msg_success'] == 'deleta'){
			$usuario = anti_injection($_GET['user']);
			echo "<script>$(function(){ $(\".alert_success font\").html(\"Usuário <span class='text-semibold'>" . $usuario . "</span> deletado com sucesso!\"); });</script>";
		}
		else if($_GET['msg_success'] == 'cria'){
			echo "<script>$(function(){ $(\".alert_success font\").html(\"Usuário <span class='text-semibold'>" . $usuario . "</span> (" . $nome . ") criado com sucesso!\"); });</script>";
		}
	}
	if(isset($_GET['msg_error'])){
		echo "<script>$(function(){ $(\".alert_error#msg" . $_GET['msg_error'] . "\").removeClass(\"hide\"); });</script>";
	}
?>

	<body class="navbar-top">
		<?php
			// Inseri a barra fixa do topo
			include("includes/navbar-top.php");
		?>
		<!-- Page container -->
		<div class="page-container">

			<!-- Page content -->
			<div class="page-content">
				<?php
					// Inseri a barra fixa do topo
					include("includes/sidebar.php");
				?>	
				<!-- Main content -->
				<div class="content-wrapper">

					<!-- Page header -->
					<div class="page-header">
						<div class="page-header-content">
							<div class="page-title">
								<h4><span class="text-semibold">Usuários</span></h4>
							</div>

							<div class="heading-elements">
								<div class="heading-btn-group">
									<a href="usuarios_novo.php"><button type="button" class="btn btn-labeled bg-teal-300"><b><i class="icon-user-plus"></i></b> Novo Usuário</button></a>
								</div>
							</div>
						</div>

					</div>
					<!-- /page header -->


					<!-- Content area -->
					<div class="content">

						<div class="row">
							<div class="col-sm-12">

								<div class="alert  alert-bordered alert-success hide alert_success">
									<button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Fechar</span></button>
									<font></font> 
							  	</div>
								<div class="alert alert-bordered alert-danger hide alert_error" id="msg1">
									<button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Fechar</span></button>
									Você não pode inativar o seu próprio usuário de acesso. 
							  	</div>
								<div class="alert alert-bordered alert-danger hide alert_error" id="msg2">
									<button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Fechar</span></button>
									Você não pode deletar o seu próprio usuário de acesso. 
							  	</div>

								<!-- Dropdown menu -->
								<h6 class="content-group text-semibold">
									Usuários ativos
								</h6>

								<div class="row">

									<?php
										$query = "SELECT * FROM usuarios WHERE status = '1' ORDER BY nome";
										$result = mysql_query($query) or die(mysql_error());
										$row = mysql_num_rows($result);

										if($row > 0){
											$cont = 0;
											while($vet = mysql_fetch_array($result)){
												if($cont == 4){
													echo '</div>';
													echo '<div class="row">';
													$cont = 0;
												}

												if($vet['img_perfil'] != ''){
													$img_perfil = "uploads/perfil/" . $vet['img_perfil'];
												}
												else{
													$img_perfil = "assets/images/user_default.jpg";
												}
									?>
									<div class="col-lg-3 col-md-6">
										<div style="background-color: #e8f5e9;" class="panel panel-body box-success text-success">
											<div class="media">
												<div class="media-left">
													<a href="<?php echo $img_perfil; ?>" data-popup="lightbox">
														<img src="<?php echo $img_perfil; ?>" class="img-circle img-lg" alt="">
													</a>
												</div>

												<div class="media-body">
													<h6 class="media-heading"><?php echo $vet['nome'];?></h6>
													<span class="text-muted"><?php echo $vet['login'];?></span>
												</div>

												<div class="media-right media-middle">
													<ul class="icons-list icons-list-vertical">
								                    	<li class="dropdown">
									                    	<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu7"></i></a>
									                    	<ul class="dropdown-menu dropdown-menu-right bg-success">
																<li><a href="usuarios_edit.php?edit=<?php echo $vet['codigo']; ?>"><i class="icon-pencil3 pull-right"></i> Alterar dados</a></li>
																<li><a href="usuarios.php?delet=<?php echo $vet['codigo']; ?>"><i class="icon-user-cancel pull-right"></i> Deletar usuário</a></li>
																<li><a href="usuarios_acessos.php?acesso=<?php echo $vet['codigo']; ?>"><i class="icon-list pull-right"></i> Ver acessos</a></li>
																<li class="divider"></li>
																<li><a href="usuarios.php?inativa=<?php echo $vet['codigo'];?>"><i class="icon-user-block pull-right"></i> Inativar usuário</a></li>
															</ul>
								                    	</li>
							                    	</ul>
												</div>
											</div>
										</div>
									</div>
									<?php
										$cont++;
											}
										}
										else{
											// Se não houver usuários ativos no banco de dados
										}
									?>
								</div>
							</div>
						</div>	
						<hr>
						<div class="row voffset">
							<div class="col-sm-12">
								<!-- Dropdown menu -->
								<h6 class="content-group text-semibold">
									Usuários inativos
									<small class="display-block">Aguardando liberação...</small>
								</h6>

								<div class="row">
									<?php
										$query = "SELECT * FROM usuarios WHERE status = '0' ORDER BY nome";
										$result = mysql_query($query) or die(mysql_error());
										$row = mysql_num_rows($result);

										if($row > 0){
											$cont = 0;
											while($vet = mysql_fetch_array($result)){
												if($cont == 4){
													echo '</div>';
													echo '<div class="row">';
													$cont = 0;
												}

												if($vet['img_perfil'] != ''){
													$img_perfil = "uploads/perfil/" . $vet['img_perfil'];
												}
												else{
													$img_perfil = "assets/images/user_default.jpg";
												}

									?>
									<div class="col-lg-3 col-md-6">
										<div style="background-color:#fbe9e7;" class="panel panel-body border-danger text-danger">
											<div class="media">
												<div class="media-left">
													<a href="<?php echo $img_perfil; ?>" data-popup="lightbox">
														<img src="<?php echo $img_perfil; ?>" class="img-circle img-lg" alt="">
													</a>
												</div>

												<div class="media-body">
													<h6 class="media-heading"><?php echo $vet['nome'];?></h6>
													<span class="text-muted text-danger-400"><?php echo $vet['login'];?></span>
												</div>

												<div class="media-right media-middle">
													<ul class="icons-list icons-list-vertical">
								                    	<li class="dropdown">
									                    	<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu7"></i></a>
									                    	<ul class="dropdown-menu dropdown-menu-right bg-danger">
																<li><a href="usuarios_edit.php?edit=<?php echo $vet['codigo']; ?>"><i class="icon-pencil3 pull-right"></i> Alterar dados</a></li>
																<li><a href="usuarios.php?delet=<?php echo $vet['codigo']; ?>"><i class="icon-user-cancel pull-right"></i> Deletar usuário</a></li>
																<li><a href="usuarios.php?ativa=<?php echo $vet['codigo'];?>"><i class="icon-user-check pull-right"></i> Ativar usuário</a></li>
															</ul>
								                    	</li>
							                    	</ul>
												</div>
											</div>
										</div>
									</div>
									<?php
										$cont++;
											}
										}
										else{
											// Se não houver usuários ativos no banco de dados
									?>
									<div class="col-lg-4 col-md-6">
										<div class="alert alert-bordered alert-warning">
											Nenhum usuário inativo no momento.
								  		</div>
								  	</div>
									<?php
										}
									?>
								</div>
							</div>
						</div>

						</div>
						<!-- /dropdown menu -->
<?php
	include("includes/footer.php");
?>