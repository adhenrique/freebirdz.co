<?php
	session_start();

	$include_js = '
	<script type="text/javascript" src="assets/js/plugins/media/fancybox.min.js"></script>

	<script type="text/javascript" src="assets/js/plugins/forms/selects/select2.min.js"></script>
	<script type="text/javascript" src="assets/js/plugins/forms/styling/uniform.min.js"></script>

	<script type="text/javascript" src="assets/js/pages/user_pages_team.js"></script>
	<script type="text/javascript" src="assets/js/pages/form_layouts.js"></script>

	<script type="text/javascript">
		$(function(){
			$("#link_sidebar_usuarios").addClass("active");
		});
	</script>';
	$include_ccs = "";
	include("includes/header.php");
	include("includes/verifica.php");


	if($_SESSION['admin'] != 1 AND $_GET['edit'] != $_SESSION['adm_codigo']){
		redireciona('dashboard.php');
		die;
	}

	if($_GET['edit']){
		$usuario_edit = anti_injection($_GET['edit']);
	}
	else{
		redireciona("usuarios.php");
	}

	if($_POST['cmd'] == "edit"){
		$usuario_edit = $_POST['usuario_edit'];

		$nome = addslashes($_POST['nome']);
		$sobrenome = addslashes($_POST['sobrenome']);
		$email = addslashes($_POST['email']);
		$login = addslashes($_POST['usuario']);
		$status = $_POST['status'];
		$admin = $_POST['admin'];

		if($usuario_edit == $_SESSION['adm_codigo']){
			$status = 1;
			$set_query = "";
		}
		else{
			$set_query = ", admin = '$admin'";
		}

		$senha = $_POST['senha'];
		$senha_repeat = $_POST['senha_repeat'];

		$img_perfil = $_FILES['foto_perfil'];

		$query_alter_dados = "UPDATE usuarios SET nome = '$nome', sobrenome = '$sobrenome', email = '$email', login = '$login', status = '$status' $set_query WHERE codigo = '$usuario_edit'";

		if($senha){
			if($senha == $senha_repeat){
				$senha = md5($senha);
				$query_alter_senha = "UPDATE usuarios SET senha = '$senha' WHERE codigo = '$usuario_edit'";
				$result_alter_senha = mysql_query($query_alter_senha) or die(mysql_error());

				if($result_alter_senha){
					$result_alter_dados = mysql_query($query_alter_dados) or die(mysql_error());
				}
			}
		}
		if($img_perfil['tmp_name']){
			$diretorio = "uploads/perfil/";
			$extensao = explode("/", $img_perfil["type"]);
			$novo_nome = $nome . "-" . $usuario_edit . "-" . date('d-m-Y-H-i-s') . "-" . mt_rand(1111,9999) . "." . $extensao[1];
			if(move_uploaded_file($img_perfil["tmp_name"], $diretorio . $novo_nome)){
				$query_consult = "SELECT * FROM usuarios WHERE codigo = '$usuario_edit'";
				$result_consult = mysql_query($query_consult) or die(mysql_error());
				$vet_consult = mysql_fetch_array($result_consult);
				$imagem_antiga = $vet_consult['img_perfil'];

				$query_alter_image = "UPDATE usuarios SET img_perfil = '$novo_nome' WHERE codigo = '$usuario_edit'";
				$result_alter_image = mysql_query($query_alter_image) or die(mysql_error());

				if(isset($result_alter_image)){
					if(!($senha)){
						$result_alter_dados = mysql_query($query_alter_dados) or die(mysql_error());
					}

					if($usuario_edit == $_SESSION['adm_codigo']){
						$_SESSION['adm_perfil'] = $diretorio . $novo_nome;
					}

					if($imagem_antiga != ''){
						unlink("uploads/perfil/" . $imagem_antiga);
					}
					echo "<script>$(function(){ $(\".alert_success#msg2\").removeClass(\"hide\"); });</script>";

				}

			}
		}

		if(!($senha) && !($img_perfil['tmp_name'])){
			$result_alter_dados = mysql_query($query_alter_dados) or die(mysql_error());
		}

		if($result_alter_dados){
			echo "<script>$(function(){ $(\".alert_success#msg1\").removeClass(\"hide\"); });</script>";

			if($usuario_edit == $_SESSION['adm_codigo']){
				$query_consult_session = "SELECT * FROM usuarios WHERE codigo = '$usuario_edit'";
				$result_consult_session = mysql_query($query_consult_session) or die(mysql_error());
				$vet_consult_session = mysql_fetch_array($result_consult_session);

				$_SESSION['adm_login'] = $vet_consult_session['login'];
				$_SESSION['adm_nome'] = $vet_consult_session['nome'];
				$_SESSION['adm_nomecompleto'] = $vet_consult_session['nome'] . " " . $vet_consult_session['sobrenome'];
			}
		}

	}
?>

<body class="navbar-top">
	<?php
		// Inseri a barra fixa do topo
		include("includes/navbar-top.php");
	?>
	<!-- Page container -->
	<div class="page-container">
		<!-- Page content -->
		<div class="page-content">
			<?php
				// Inseri a barra fixa do topo
				include("includes/sidebar.php");
			?>	
			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Page header -->
				<div class="page-header">
					<div class="page-header-content">
						<div class="page-title">
							<h4><span class="text-semibold">Usuários</span> - Alterar dados</h4>
						</div>
					</div>
					<div class="breadcrumb-line">
						<ul class="breadcrumb">
							<li><a href="usuarios.php"><i class="icon-users4 position-left"></i> Usuários</a></li>
							<li class="active">Alterar dados</li>
						</ul>
					</div>

				</div>
				<!-- /page header -->


				<!-- Content area -->
				<div class="content">

					<div class="row">
						<div class="col-sm-12">
							<div class="alert  alert-bordered alert-success hide alert_success" id="msg1">
								<button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Fechar</span></button>
								Alterações dos dados do usuário realizadas com sucesso! 
						  	</div>
							<div class="alert  alert-bordered alert-success hide alert_success" id="msg2">
								<button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Fechar</span></button>
								Imagem de perfil do usuário alterada com sucesso! 
						  	</div>
							<div class="panel">
								<form method="post" class="form-horizontal" enctype="multipart/form-data">
									<input type="hidden" name="cmd" value="edit" />
									<input type="hidden" name="usuario_edit" value="<?php echo $usuario_edit; ?>" />
									<?php
										$query = "SELECT * FROM usuarios WHERE codigo = '$usuario_edit'";
										$result = mysql_query($query) or die(mysql_error());
										$vet = mysql_fetch_array($result);

										if($vet['img_perfil'] != ''){
											$img_perfil = "uploads/perfil/" . $vet['img_perfil'];
										}
										else{
											$img_perfil = "assets/images/user_default.jpg";
										}
									?>
									<div class="panel-body">
										<div class="col-sm-6">
											<div class="form-group">
												<label class="col-lg-3 control-label">Nome:</label>
												<div class="col-lg-9">
													<input type="text" name="nome" class="form-control" placeholder="" value="<?php echo $vet['nome']; ?>">
												</div>
											</div>
										</div>
										<div class="col-sm-6">
											<div class="form-group">
												<label class="col-lg-3 control-label">Sobrenome:</label>
												<div class="col-lg-9">
													<input type="text" name="sobrenome" class="form-control" placeholder="" value="<?php echo $vet['sobrenome']; ?>">
												</div>
											</div>
										</div>
										<div class="col-sm-6">
											<div class="form-group">
												<label class="col-lg-3 control-label">Usúario:</label>
												<div class="col-lg-9">
													<input type="text" name="usuario" class="form-control" placeholder="" value="<?php echo $vet['login']; ?>">
												</div>
											</div>
										</div>
										<div class="col-sm-6">
											<div class="form-group">
												<label class="col-lg-3 control-label">E-mail:</label>
												<div class="col-lg-9">
													<input type="email" name="email" class="form-control" placeholder="" value="<?php echo $vet['email']; ?>">
												</div>
											</div>
										</div>
										<div class="col-sm-6">
											<div class="form-group">
												<label class="col-lg-3 control-label">Nova Senha:</label>
												<div class="col-lg-9">
													<input type="password" name="senha" class="form-control" placeholder="">
												</div>
											</div>
										</div>
										<div class="col-sm-6">
											<div class="form-group">
												<label class="col-lg-3 control-label">Repita a Senha:</label>
												<div class="col-lg-9">
													<input type="password" name="senha_repeat" class="form-control" placeholder="">
												</div>
											</div>
										</div>
										<div class="col-sm-6">
											<div class="form-group">
												<label class="col-lg-3 control-label">Foto de Perfil:</label>
												<div class="col-lg-9">
													<div class="media no-margin-top">
														<div class="media-left">
															<img src="<?php echo $img_perfil; ?>" style="width: 58px; height: 58px;" class="img-rounded" alt="">
														</div>

														<div class="media-body">
															<input type="file" name="foto_perfil" class="file-styled">
															<span class="help-block">Formatos suportados: gif, png, jpg. Tamanho máximo 2Mb</span>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="col-sm-6">
											<div class="form-group" id="radio_status">
												<script type="text/javascript">
													$(function(){
														<?php
															if($usuario_edit == $_SESSION['adm_codigo']){
																echo '$("#radio_status .radio-inline, #radio_status .radio-inline .choice").addClass("disabled");';
																echo '$("#radio_status .radio-inline .choice input").attr("disabled", "disabled");';
															}
														?>
														
													});
												</script>
												<label class="col-lg-3 control-label">Status:</label>
												<div class="col-lg-9">
													<label class="radio-inline">
														<input type="radio" class="styled" name="status" <?php if($vet['status'] == "1"){ echo "checked = \"checked\"";} ?> value="1">
														Ativo
													</label>

													<label class="radio-inline">
														<input type="radio" class="styled" name="status" <?php if($vet['status'] == "0"){ echo "checked = \"checked\"";} ?> value="0">
														Inativo
													</label>
												</div>
											</div>
										</div>
										<div class="col-sm-6">
											<div class="form-group" id="radio_status">
												<label class="col-lg-3 control-label">Administrador:</label>
												<div class="col-lg-9">
													<label class="radio-inline">
														<input type="radio" class="styled" name="admin" <?php if($vet['admin'] == "1"){ echo "checked = \"checked\"";} ?>  value="1" checked="checked">
														Sim
													</label>

													<label class="radio-inline">
														<input type="radio" class="styled" name="admin" <?php if($vet['admin'] == "0"){ echo "checked = \"checked\"";} ?>  value="0">
														Não
													</label>
												</div>
											</div>
										</div>

										<div class="col-sm-12">
											<div class="text-right">
												<button type="submit" class="btn bg-teal-300">Alterar <i class="icon-arrow-right14 position-right"></i></button>
											</div>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>	

					</div>
					<!-- /dropdown menu -->
<?php
	include("includes/footer.php");
?>