<?php
	session_start();

	$include_js = '

	<script type="text/javascript" src="assets/js/plugins/media/fancybox.min.js"></script>
	<script type="text/javascript" src="assets/js/pages/gallery.js"></script>

	<script type="text/javascript" src="assets/js/plugins/forms/selects/select2.min.js"></script>
	<script type="text/javascript" src="assets/js/plugins/forms/styling/uniform.min.js"></script>

	<script type="text/javascript" src="assets/js/pages/form_layouts.js"></script>


	<script type="text/javascript">
		$(function(){
			$("#link_sidebar_textos_home").addClass("active");
		});
	</script>';

	$include_ccs = "";
	include("includes/header.php");
	include("includes/verifica.php");

	if($_POST['cmd'] == "edit"){

		$texto_topo = str_replace("'", "\'", $_POST['texto_topo']);
		$texto_portfolio = str_replace("'", "\'", $_POST['texto_portfolio']);
		$titulo_newsletter = str_replace("'", "\'", $_POST['titulo_newsletter']);
		$texto_newsletter = str_replace("'", "\'", $_POST['texto_newsletter']);

		$texto_topo_en = str_replace("'", "\'", $_POST['texto_topo_en']);
		$texto_portfolio_en = str_replace("'", "\'", $_POST['texto_portfolio_en']);
		$titulo_newsletter_en = str_replace("'", "\'", $_POST['titulo_newsletter_en']);
		$texto_newsletter_en = str_replace("'", "\'", $_POST['texto_newsletter_en']);

		$query_alter_dados = "UPDATE textos_index SET texto_topo = '$texto_topo', texto_portfolio = '$texto_portfolio', titulo_newsletter = '$titulo_newsletter', texto_newsletter = '$texto_newsletter', texto_topo_en = '$texto_topo_en', texto_portfolio_en = '$texto_portfolio_en', titulo_newsletter_en = '$titulo_newsletter_en', texto_newsletter_en = '$texto_newsletter_en'";
		$result_alter_dados = mysql_query($query_alter_dados) or die(mysql_error());


		redireciona("textos-home.php");
	}


	$query = "SELECT * FROM textos_index";
	$result = mysql_query($query) or die (mysql_error());
	$vet_textos = mysql_fetch_array($result);
				

?>

<body class="navbar-top">
	<?php
		// Inseri a barra fixa do topo
		include("includes/navbar-top.php");
	?>
	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">
			<?php
				// Inseri a barra fixa do topo
				include("includes/sidebar.php");
			?>	
			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Page header -->
				<div class="page-header">
					<div class="page-header-content">
						<div class="page-title">
							<h4><span class="text-semibold">Textos da Home</span></h4>
						</div>
					</div>

				</div>
				<!-- /page header -->


				<!-- Content area -->
				<div class="content">
					<div class="row">
						<div class="col-sm-12">
							<div class="panel">
								<div class="panel-body">
									<form method="post" class="form-horizontal" id="form1">
										<input type="hidden" name="cmd" value="edit" />

										<div class="col-sm-12">	
											<h5 class="panel-title">Topo do Site (nuvens)</h5>
											<br>
											<label class="text-right control-label"><img src="assets/images/baneira-pt.png" width="20px" style="position: relative; top: -1px;" /> Texto Topo:</label>
											<textarea name="texto_topo" class="form-control" style="resize: none;" rows="4" required><?=$vet_textos['texto_topo'];?></textarea>
											<br>
											<label class="text-right control-label"><img src="assets/images/baneira-en.png" width="20px" style="position: relative; top: -1px;" /> Texto Topo:</label>
											<textarea name="texto_topo_en" class="form-control" style="resize: none;" rows="4" required><?=$vet_textos['texto_topo_en'];?></textarea>
										</div>

										<div class="col-sm-12">
											<br><hr><br>
											<h5 class="panel-title">Portfólio</h5>
											<br>

											<label class="text-right control-label"><img src="assets/images/baneira-pt.png" width="20px" style="position: relative; top: -1px;" /> Texto Portfólio:</label>
											<textarea name="texto_portfolio" class="form-control" style="resize: none;" rows="4" required><?=$vet_textos['texto_portfolio'];?></textarea>
											<br>
											<label class="text-right control-label"><img src="assets/images/baneira-en.png" width="20px" style="position: relative; top: -1px;" /> Texto Portfólio:</label>
											<textarea name="texto_portfolio_en" class="form-control" style="resize: none;" rows="4" required><?=$vet_textos['texto_portfolio_en'];?></textarea>	
										</div>

										<div class="col-sm-12">	
											<br><hr><br>
											<h5 class="panel-title">Formulário Newsletter</h5>
											<br>

											<label class="text-right control-label"><img src="assets/images/baneira-pt.png" width="20px" style="position: relative; top: -1px;" /> Título Newsletter:</label>
											<input type="text" name="titulo_newsletter" class="form-control" value="<?=$vet_textos['titulo_newsletter'];?>" required />
											<br>
											<label class="text-right control-label"><img src="assets/images/baneira-en.png" width="20px" style="position: relative; top: -1px;" /> Título Newsletter:</label>
											<input type="text" name="titulo_newsletter_en" class="form-control" value="<?=$vet_textos['titulo_newsletter_en'];?>" required />
											<br>
											<label class="text-right control-label"><img src="assets/images/baneira-pt.png" width="20px" style="position: relative; top: -1px;" /> Texto Newsletter:</label>
											<textarea name="texto_newsletter" class="form-control" style="resize: none;" rows="4" required><?=$vet_textos['texto_newsletter'];?></textarea>
											<br>
											<label class="text-right control-label"><img src="assets/images/baneira-en.png" width="20px" style="position: relative; top: -1px;" /> Texto Newsletter:</label>
											<textarea name="texto_newsletter_en" class="form-control" style="resize: none;" rows="4" required><?=$vet_textos['texto_newsletter_en'];?></textarea>
										</div>

										<div class="col-sm-12">
											<br><br>
											<div class="text-right">
												<button type="submit" class="btn bg-teal-300">Alterar <i class="icon-arrow-right14 position-right"></i></button>
											</div>
										</div>

									</form>
								</div>
							</div>
						</div>
					</div>	

				</div>

<?php
	include("includes/footer.php");
?>