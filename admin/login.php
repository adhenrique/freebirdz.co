<?php
	session_start();

	$include_ccs = '';
	include("includes/header.php");

	if($_SESSION['adm_verifica'] == 'adm_logado'){
		redireciona("dashboard.php");
		die;
	}

	if($_POST['cmd'] == "logar"){
		
		$usuario = anti_injection($_POST['usuario']);
		$senha = $_POST['senha'];
		$senha_md5 = md5($senha);

		$query = "SELECT * FROM usuarios WHERE login = '$usuario' AND senha = '$senha_md5'";
		$result = mysql_query($query) or die(mysql_error());
		$row = mysql_num_rows($result);

		if($row > 0){
			$vet = mysql_fetch_array($result);
			if($vet['status'] == '0'){
				redireciona("login.php?login=inativo");
				die;
			}

			$idusuario = $vet['codigo'];
			$usuario = $vet['login'];
			$nome = $vet['nome'];
			$nomecompleto = $vet['nome'] . ' ' . $vet['sobrenome'];
			$foto_perfil = $vet['img_perfil'];
			$admin = $vet['admin'];

			$ip = $_SERVER['REMOTE_ADDR'];

			$_SESSION['adm_verifica'] = "adm_logado";
			$_SESSION['adm_login'] = $usuario;
			$_SESSION['adm_nome'] = $nome;
			$_SESSION['adm_nomecompleto'] = $nomecompleto;
			$_SESSION['admin'] = $admin;

			if($foto_perfil != ''){
				$_SESSION['adm_perfil'] = "uploads/perfil/" . $foto_perfil;
			}
			else{
				$_SESSION['adm_perfil'] = "assets/images/user_default.jpg";
			}
			
			$_SESSION['adm_codigo'] = $idusuario;

			$query = "SELECT * FROM acessos WHERE idusuario = '$idusuario' ORDER BY codigo DESC LIMIT 1";
			$result = mysql_query($query) or die(mysql_error());
			$row = mysql_num_rows($result);
			if($row > 0){
				$vet = mysql_fetch_array($result);
				$acesso_num = $vet['acesso_num'] + 1;
			}
			else{
				$acesso_num = 1;
			}

			$query = "INSERT INTO acessos (idusuario, ip, data, acesso_num) VALUE ('$idusuario', '$ip', '$data_hora_eua', '$acesso_num')";
			$result = mysql_query($query) or die(mysql_error());

			redireciona("dashboard.php");

		}
		else{
			redireciona("login.php?login=error");
		}
	}

	if($_GET['login'] == 'error'){
		echo "<script>$(function(){ $(\".alert_login_error\").removeClass(\"hide\");});</script>";
	}
	if($_GET['login'] == 'inativo'){
		echo "<script>$(function(){ $(\".alert_login_inativo\").removeClass(\"hide\");});</script>";
	}
?>

<body>

	<!-- Page container -->
	<div class="page-container login-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Content area -->
				<div class="content">

					<!-- Simple login form -->
					<form method="post">
						<input type="hidden" name="cmd" value="logar" />
						<input type="hidden" id="local_cidade" name="cidade" value="" />
						<input type="hidden" id="local_estado" name="estado" value="" />
						<input type="hidden" id="local_pais" name="pais" value="" />
						<div class="panel panel-body login-form">
							<div class="text-center">
								<div class="row">
									<div class="col-sm-10 col-sm-offset-1">
										<img src="assets/images/logo.png" class="img-responsive" />
									</div>
								</div>
								<br>
								<h5 class="content-group">Faça login em sua conta <small class="display-block">Entre com seu usúario e senha</small></h5>
							</div>
							<div class="alert alert-danger alert-bordered hide alert_login_error">
								<button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Fechar</span></button>
								<span class="text-semibold">Usuário e/ou Senha Incorretos!</span> 
						    </div>
							<div class="alert alert-warning alert-bordered hide alert_login_inativo">
								<button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Fechar</span></button>
								<span class="text-semibold">Usuário inativo!</span> 
						    </div>
							<div class="form-group has-feedback has-feedback-left">
								<input type="text" name="usuario" class="form-control" placeholder="Usuário">
								<div class="form-control-feedback">
									<i class="icon-user text-muted"></i>
								</div>
							</div>

							<div class="form-group has-feedback has-feedback-left">
								<input type="password" name="senha" class="form-control" placeholder="Senha">
								<div class="form-control-feedback">
									<i class="icon-lock2 text-muted"></i>
								</div>
							</div>

							<div class="form-group">
								<button type="submit" class="btn bg-teal-300 btn-block">Acessar <i class="icon-circle-right2 position-right"></i></button>
							</div>

							<!--
							<div class="text-center">
								<a href="esqueceu_senha.php">Esqueceu sua senha?</a>
							</div>
							-->
						</div>
					</form>
					<!-- /simple login form -->


					<!-- Footer -->
					<div class="footer text-muted">
						<!-- &copy; 2016. <a href="#">Limitless Web App Kit</a> by <a href="http://themeforest.net/user/Kopyov" target="_blank">Eugene Kopyov</a> -->
					</div>
					<!-- /footer -->

				</div>
				<!-- /content area -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->

	<script>
		// Localização do usuário a logar 
		var fillInPage = (function () {
			var updateCityText = function (geoipResponse) {

				var link = "/shelter?lat=" + geoipResponse.location.latitude;
				link += "&lon=" +geoipResponse.location.longitude;

				/* Cidade */
				var cityName = geoipResponse.city.names.es;
				$("input#local_cidade").val(cityName);

				/* Estado */
				var regionName = geoipResponse.most_specific_subdivision.names.es;
				$("input#local_estado").val(regionName);

				/* País */
				var countryName = geoipResponse.country.names.es;
				$("input#local_pais").val(countryName);

			};

			var onSuccess = function (geoipResponse) {
				updateCityText(geoipResponse);
			};

			/* If we get an error we will */
			var onError = function (error) {
				return;
			};

			return function () {
				geoip2.city( onSuccess, onError );
			};
		}());
		fillInPage();
    </script>

</body>
</html>
