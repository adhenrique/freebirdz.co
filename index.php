<?
	include("db_acesso.php");
	include("funcoes.php");

$language = "br";
	$page = "index";
	$title = "";
	include("includes/header.php");
?>

	<script>

		$(function(){
			$.stellar({
				horizontalScrolling: false,
				verticalOffset: 15
			});
		})

	</script>

	<div class="bloco index" data-stellar-background-ratio="0.3" id="topo">
		<div class="ruido"></div>
		<div class="bar-topo">
			<div class="container-fluid">
				<div class="row barras">
					<div class="col-sm-4 col-xs-4 item amarelo"></div>
					<div class="col-sm-4 col-xs-4 item vermelho"></div>
					<div class="col-sm-4 col-xs-4 item azul"></div>
				</div>
				<?php include_once "parts/top_social_icons.php" ?>
			</div>
		</div>
		<div class="textos wow fadeIn" data-wow-delay=".4s" data-wow-duration="2.5s">
			<div class="container">
				<div class="row">
					<div class="col-sm-8 col-sm-offset-2 text-center">
						<img src="img/logo_topo.svg" class="svg img-responsive center-block logo" />
						<!--<h2><?=$vet_textos_index['titulo_topo'];?></h2>-->
						<p>
							<b>Seja bem-vindo.</b><br>
							Somos um núcleo criativo estruturado por três pilares.
						</p>
						<h2><font class="video">VIDEO</font>|<font class="design">DESIGN</font>|<font class="musica">MUSICA</font></h2>
						<a href="javascript:;" onclick="scrollPage('#portfolio');" class="mouse wow fadeInDown" data-wow-delay=".4s" data-wow-duration="3s"><img src="img/icon-mouse-scroll.gif" class="center-block" /></a>
					</div>
				</div>
			</div>
		</div>
		<div class="nuvens">
			<div class="pretas">
				<img src="img/nuvens-pretas.png" />
			</div>
			<div class="cinzas">
				<img src="img/nuvens-cinzas.png" />
			</div>
			<div class="brancas">
				<img src="img/nuvens-brancas.png" />
			</div>
		</div>
	</div>

	<div class="bloco index" id="portfolio">
		<div class="container">
			<div class="row">
				<div class="col-sm-10 col-sm-offset-1">
					<div class="title text-center wow fadeIn" data-wow-delay=".5s" data-wow-duration="1.6s">
						<h2>PROJ3TOS</h2>
						<!--<img src="img/title-portfolio.png" />-->
						<p>
							<?=nl2br(str_replace("  ", "&nbsp;&nbsp;", $vet_textos_index['texto_portfolio']));?>
						</p>
					</div>
				</div>
			</div>
			<?php include_once "parts/gallery.php" ?>
		</div>
	</div>

    <script>
        $(document).ready(function () {
            modalAlerta('', '<?=$vet_textos_index['texto_topo'];?>', '', '#modal-alerta-construcao');
        });
    </script>

<?
	include("includes/footer.php");
?>
